package com.example.shaildesai.jaza.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;
import android.widget.Toast;

import com.example.shaildesai.jaza.R;
import com.example.shaildesai.jaza.Sql.UserDatabaseHelper;
import com.google.zxing.client.android.Intents;

public class HomeMenu extends AppCompatActivity {
    private final AppCompatActivity activity = HomeMenu.this;


    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static final int scan_activity_request_code = 0;
    public String getdata;
    public UserDatabaseHelper userdatabasehelper;


    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_menu);
        userdatabasehelper = new UserDatabaseHelper(this);
       // LoginuserDailog();



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            switch (position) {

                case 1:
                    UserProfile up = new UserProfile();
                    return up;
                case 2:
                    AttendentMenu am = new AttendentMenu();
                    return am;
                default:
                    return null;
            }
        }


        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }


        @Override
        public CharSequence getPageTitle(int position) {

            switch (position) {
                case 0:
                    return "Customer Menu";
                case 1:
                    return "New Customer";
                case 2:
                    return "Attendant Menu";

            }
            return null;
        }


    }


   /* public void LoginuserDailog(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

        // set title
        alertDialogBuilder.setTitle(getString(R.string.login_Customer));

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.click_login_to_open_datamatrix_scanner_to_login)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.login),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent i = new Intent(getApplicationContext(), ScanDataMatrix.class);
                        startActivityForResult(i, scan_activity_request_code);

                            // if this button is clicked, close
                        // current activity

                    }
                })
                .setNegativeButton(getString(R.string.cancel),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                       activity.finish();

                        //bye bye
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void onActivityResult(int loginCode, int resultCode, Intent i) {
        if (scan_activity_request_code == loginCode){
            if (resultCode == RESULT_OK) {
                getdata = i.getStringExtra("scanned_id");
                userdatabasehelper.checkUser(getdata);
                if (userdatabasehelper.checkUser(getdata))
                {

                    Toast.makeText(getApplicationContext(), "Hey welcome" + userdatabasehelper.first_name , Toast.LENGTH_SHORT).show();
                }
                    else{
                        Toast.makeText(getApplicationContext(),"Sorry You are not a member sign in as tourist",Toast.LENGTH_SHORT).show();

                    }

                }



            }

        }*/

    }


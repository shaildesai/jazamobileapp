package com.jaza.shaildesai.jaza.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

public class AddOfferings extends AppCompatActivity implements View.OnClickListener {

    private EditText offering_id;
    private EditText offering_type;
    private EditText offering_name;
    private EditText price;
    private EditText mdownpayment;
    private EditText duration;
    private UserDatabaseHelper offeringsDatabaseHelper;
    private Offerings offerings;

    private Button store_offering;
    private String getdata;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_offerings);
        initViews();
        initObjects();


    }

    public void initViews() {
        offering_id = (EditText) findViewById(R.id.offerinid);
        offering_name = (EditText) findViewById(R.id.offeringname);
        price = (EditText)findViewById(R.id.price);
        mdownpayment = (EditText) findViewById(R.id.midownpayment);
        duration = (EditText) findViewById(R.id.duration);
        offering_type = findViewById(R.id.offeriningtype);

        store_offering = (Button) findViewById(R.id.storeofferings);


    }

    private void initObjects() {
        offeringsDatabaseHelper = new UserDatabaseHelper(this);
        store_offering.setOnClickListener(this);

        offerings = new Offerings();


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.storeofferings:
                postDataToofferingsTable();
                break;
        }
    }

    private void postDataToofferingsTable() {
        if (!offeringsDatabaseHelper.checkofferings(offering_name.getText().toString().trim())) {

            offerings.setOffering_id(offering_id.getText().toString().trim());
            offerings.setOffering_name(offering_name.getText().toString().trim());
            offerings.setOffering_type(offering_type.getText().toString().trim());
            offerings.setPrice(price.getText().toString().trim());
            offerings.setMin_down_payment(mdownpayment.getText().toString().trim());
            offerings.setDuration(duration.getText().toString().trim());
            offeringsDatabaseHelper.addofferings(offerings);

            Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Not inserted", Toast.LENGTH_SHORT).show();

        }
    }



}

package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.SharedPreferences;


import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Attendent_Session;
import com.jaza.shaildesai.jaza.Model.HubProperties;
import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;

public class AttendentMenu extends AppCompatActivity implements View.OnClickListener {

    private Button customer_menu;
    private Button customer_list;
    private Button hub_wallet;
    private Button jaza_pack_list;
    private Button inventory;
    private Button addjazapack;
    private Button registeruser;
    private Button cashathub;
    private UserDatabaseHelper userdatabasehelper;
    private User user;
    private static final int scan_attendent = 0;
    private String getresult;
    SharedPreferences sharedpreferencesattendent;
    public String message1;
    public SharedPreferences.Editor editorattendent;
    public static final String AttendentPref = "attendentprefs";
    public String LoginKey = "isUserlogin";
    public static final String Attendent = "attendentKey";
    private long timestamp;
    private  Attendent_Session attendent_session;
    private String event_type_enum = "9";
    private  String attendent_id;
    private StringBuilder csvList;
    private  String csv;
    private String SEPERATOR = ",";
    private Helper helper;
    private String device_id;





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendent_menu);


        if (getSharedPreferences(AttendentPref, 0).getBoolean("isLoginKey", true)) {
            initViews();

            sharedpreferencesattendent= this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
            attendent_id = sharedpreferencesattendent.getString(Attendent, "");
            device_id = sharedpreferencesattendent.getString(IMEI,"");
            initObjects();
        } else {
                    finish();
        }

    }

    private void initViews() {
        customer_menu = findViewById(R.id.customermenu);
        customer_list = findViewById(R.id.customerLists);
        hub_wallet = findViewById(R.id.hubwallet);
        jaza_pack_list = findViewById(R.id.jazapacklist);
        inventory = findViewById(R.id.inventory);
        addjazapack = findViewById(R.id.logout);
        cashathub = findViewById(R.id.cash_at_hub);
        registeruser = findViewById(R.id.register_customer);

    }

    private void initObjects() {
        customer_list.setOnClickListener(this);
        customer_menu.setOnClickListener(this);
        hub_wallet.setOnClickListener(this);
        jaza_pack_list.setOnClickListener(this);
        inventory.setOnClickListener(this);
        addjazapack.setOnClickListener(this);
        userdatabasehelper = new UserDatabaseHelper(this);
        cashathub.setOnClickListener(this);
        registeruser.setOnClickListener(this);
        User user = new User();
        attendent_session = new Attendent_Session();
        helper = new Helper(this);
        if(!attendent_id.equals("")) {
            if (!getSharedPreferences(AttendentPref, 0).getBoolean("isLoginKey", true)) {
                storeattendentsessionlogin();
            }
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.customermenu:
                launchCustomerMenu();
                break;
            case R.id.customerLists:
                launchCustomerList();
                break;
            case R.id.hubwallet:
                launcHubProperties();
                break;
            case R.id.jazapacklist:
                break;
            case R.id.inventory:
                break;
            case R.id.logout:
                logoutsharedpreference();
                break;
            case R.id.register_customer:
                launchRegistrationPage();
                break;
            case R.id.cash_at_hub:
                launchHubWallet();

                break;


        }
    }

    public void launchCustomerMenu(){
        Intent intent = new Intent(this,MainActivity.class);
        startActivity(intent);


    }

    public void launchCustomerList(){
        Intent intent = new Intent(this,CustomerList.class);
        startActivity(intent);
    }

    public void launchRegistrationPage(){
        Intent intent = new Intent(this,RegisterUser.class);
        startActivity(intent);
    }


    public void launcHubProperties(){
        Intent intent = new Intent(this,HubPropertiesMenu.class);
        startActivity(intent);

    }
    public void launchHubWallet(){
        Intent intent = new Intent(this,HubWallet.class);
        startActivity(intent);
        finish();

    }

    public void logoutsharedpreference() {
       sharedpreferencesattendent= this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferencesattendent.edit();
        editor.putBoolean(LoginKey,false);
        editor.clear();
        editor.commit();
        finish();


    }

    public void storeattendentsessionlogin(){

        timestamp = System.currentTimeMillis() / 1000L;
        attendent_session.setAtt_id(attendent_id);
        attendent_session.setTime_in(String.valueOf(timestamp));
        userdatabasehelper.addAttendentSession(attendent_session);
        helper.storetranspublish(createLoginattendentString(),device_id);

    }

    public String createLoginattendentString() {
        List<String> dataList = new ArrayList<>();

        String publishid = String.valueOf(userdatabasehelper.lastpub());
        String eventtype = event_type_enum;
        timestamp = System.currentTimeMillis() / 1000L;

        String offeringversionid = String.valueOf(userdatabasehelper.maxofferingversionid());
        String attendentid = attendent_id;


        dataList.add(publishid);
        dataList.add(eventtype);
        dataList.add(String.valueOf(timestamp));
        dataList.add(offeringversionid);
        dataList.add(attendentid);


        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;

    }
    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, MainActivity.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }



}




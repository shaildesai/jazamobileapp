package com.jaza.shaildesai.jaza.Activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class BaseActivity extends AppCompatActivity {

    public static final long TIMEOUT_IN_MILLI = 50000 ;
    public static final long TIMEOUT_BUffer = 5000 ;
    public static final String PREF_FILE = "attendentprefs";
    public static final String KEY_SP_LAST_INTERACTION_TIME = "KEY_SP_LAST_INTERACTION_TIME";
    public String LoginKey = "isUserlogin";
    private Context context;


    private Timer timer;
    private Timer timer2;
    TimerTask mTimerTask;
    final Handler handler = new Handler();
    private Runnable runnable;
    final Handler handler2 = new Handler();
    private Runnable runnable2;


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

          /*  new Handler().postDelayed(new Runnable(){
                @Override
                public void run() {
                    try {
                        conformationDailog();
                    } catch (Exception e) {

                    }
                }

            }, 50000);

        }
        */

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {

                    conformationDailog();
            }
        };
        handler.postDelayed(myRunnable, 50000);
    }




        /*if (isValidLogin()) {
            getSharedPreference().edit().putLong(KEY_SP_LAST_INTERACTION_TIME, System.currentTimeMillis()).apply();
            for (int i = 0; i < 1; i++) {
                Log.i("hey I am here", String.valueOf(System.currentTimeMillis()));
            }
        }
        else {
            conformationDailog();
            startUserSession();
        }
        }*/


   /* public SharedPreferences getSharedPreference() {
        for(int i = 0; i < 100; i++) {
            Log.i("hey I am here", "I am here in shared pref");
        }
        return getSharedPreferences(PREF_FILE, MODE_PRIVATE);

    }*/

   /* public boolean isValidLogin() {
        long last_edit_time = getSharedPreference().getLong(KEY_SP_LAST_INTERACTION_TIME, 0);
        if(System.currentTimeMillis() - last_edit_time < TIMEOUT_IN_MILLI){
            for(int i = 0; i < 100; i++) {
                Log.i("true", String.valueOf(System.currentTimeMillis() - last_edit_time));
            }
        }
        return last_edit_time == 0 || System.currentTimeMillis() - last_edit_time < TIMEOUT_IN_MILLI;
    }*/

    public void conformationDailog(){
        startUserSession();

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        startUserSession();
        // set title
        alertDialogBuilder.setTitle("Your Title");

        // set dialog message
        alertDialogBuilder
                .setMessage("To continue you session press yes")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        handler2.removeCallbacks(runnable2);
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {

                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logout() {

        SharedPreferences sharedpreferencesattendent = this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferencesattendent.edit();
        editor.putBoolean(LoginKey,false);
        editor.clear();
        editor.commit();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
        Toast.makeText(this, "User logout due to inactivity", Toast.LENGTH_SHORT).show();
        //getSharedPreference().edit().remove(KEY_SP_LAST_INTERACTION_TIME).apply(); // make shared preference null.
    }


    public void startUserSession() {
         runnable = new Runnable() {
            @Override
            public void run() {

                    logout();
            }

        };
        handler2.postDelayed(runnable2, 5000);
    }




}


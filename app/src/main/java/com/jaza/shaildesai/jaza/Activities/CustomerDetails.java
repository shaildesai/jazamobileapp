package com.jaza.shaildesai.jaza.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

public class CustomerDetails extends AppCompatActivity {

    public TextView first_name;
    public TextView last_name;
    public TextView phone_number;
    public TextView birth_year;
    public UserDatabaseHelper userDatabaseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details);
        initViews();
        initObjects();

        User user = (User) getIntent().getExtras().getSerializable("CUSTOMER");

        first_name.setText(user.getFirst_name());
        last_name.setText(user.getLast_name());
        phone_number.setText(user.getPhone_number());
        birth_year.setText(user.getBirth_year());

    }

    public void initViews(){
        first_name = findViewById(R.id.firstname);
        last_name = findViewById(R.id.lastname);
        phone_number = findViewById(R.id.phonenumber);
        birth_year = findViewById(R.id.birthyear);
    }
    public  void initObjects()
    {
        userDatabaseHelper = new UserDatabaseHelper(this);
    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, CustomerMenu.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }





}

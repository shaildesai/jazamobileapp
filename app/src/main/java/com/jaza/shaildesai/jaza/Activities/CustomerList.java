package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class CustomerList extends AppCompatActivity implements View.OnClickListener {

    private UserDatabaseHelper userDatabaseHelper;
    private User user;
    private ListView listView;
    private String userid;
    List<User> users;
    ArrayAdapter<User> arrayAdapter;
    private EditText searchbox;
    private Button search;
    private ListView saerchlist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_list);
        userDatabaseHelper = new UserDatabaseHelper(this);
        searchbox = findViewById(R.id.Searchcustomer);
        search = findViewById(R.id.searchthis);
        saerchlist = findViewById(R.id.searchlist);
        saerchlist.setVisibility(View.GONE);



        userDatabaseHelper = new UserDatabaseHelper(this);
        search.setOnClickListener(this);
        SharedPreferences sharedpreferences = this.getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        userid = sharedpreferences.getString(Name,"");


        listView = findViewById(R.id.listcustomers);

        users = userDatabaseHelper.getallusers();
        arrayAdapter = new ArrayAdapter<User>(this,android.R.layout.simple_expandable_list_item_1, users){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Cast the list view each item as text view
                TextView item = (TextView) super.getView(position,convertView,parent);

                // Set the typeface/font for the current item

                // Set the list view item's text color
                item.setTextColor(Color.parseColor("#21CEC9"));

               // item.setTypeface(item.getTypeface(), Typeface.BOLD);

                item.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);

                item.setGravity(Gravity.CENTER);



                // return the view
                return item;
            }
        };


        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = users.get(position);

                Intent intent = new Intent(CustomerList.this, CustomerDetails.class);
                intent.putExtra("CUSTOMER", user);
                startActivity(intent);

            }
        });
    }



    private void searchcustomer()
    {

        saerchlist.setVisibility(View.VISIBLE);
        users = userDatabaseHelper.getsearcheduser(searchbox.getText().toString());
        listView.setVisibility(View.GONE);
        arrayAdapter = new ArrayAdapter<User>(this,android.R.layout.simple_expandable_list_item_1, users){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Cast the list view each item as text view
                TextView item = (TextView) super.getView(position,convertView,parent);

                // Set the typeface/font for the current item

                // Set the list view item's text color
                item.setTextColor(Color.parseColor("#21CEC9"));

                // item.setTypeface(item.getTypeface(), Typeface.BOLD);

                item.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);

                item.setGravity(Gravity.CENTER);



                // return the view
                return item;
            }
        };


        saerchlist.setAdapter(arrayAdapter);
       saerchlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = users.get(position);

                Intent intent = new Intent(CustomerList.this, CustomerDetails.class);
                intent.putExtra("CUSTOMER", user);
                startActivity(intent);

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.searchthis:
                searchcustomer();
                break;

        }

    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, AttendentMenu.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }
}


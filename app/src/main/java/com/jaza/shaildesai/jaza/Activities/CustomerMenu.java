package com.jaza.shaildesai.jaza.Activities;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class CustomerMenu extends AppCompatActivity implements View.OnClickListener {
    private Button buyjazakit;
    private Button commodity;
    private Button jazapackswap;
    private Button freetrial;
    private Button membership;
    private Button exchange;
    private Button loanpayment;
    private Button logout;
    private Button help;
    private Button usbcharge;

    //User profile
    public String userid;
    public UserDatabaseHelper userDatabaseHelper;
    public ListView listView;
    List<User> users;
    ArrayAdapter<User> arrayAdapter;
    private final  CustomerMenu activity = CustomerMenu.this;


    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_menu);
        getSupportActionBar().hide();
        SharedPreferences sharedpreferences = this.getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        userid = sharedpreferences.getString(Name,"");
        Toast.makeText(this, userid, Toast.LENGTH_SHORT).show();
        launchTestService();


        

        buyjazakit=(Button) findViewById(R.id.buyjazakit);
        usbcharge = (Button) findViewById(R.id.usbcharge);
        commodity = (Button) findViewById(R.id.comodity);
        jazapackswap = (Button) findViewById(R.id.jazapackswap);
        freetrial = (Button) findViewById(R.id.freetrial);
        membership = (Button) findViewById(R.id.membership);
        exchange = (Button) findViewById(R.id.exchange);
        loanpayment = (Button) findViewById(R.id.loanpayment);
        logout = (Button) findViewById(R.id.logout);
        help = (Button) findViewById(R.id.help);

        buyjazakit.setOnClickListener(this);
        usbcharge.setOnClickListener(this);
        commodity.setOnClickListener(this);
        jazapackswap.setOnClickListener(this);
        freetrial.setOnClickListener(this);
        membership.setOnClickListener(this);
        exchange.setOnClickListener(this);
        loanpayment.setOnClickListener(this);
        logout.setOnClickListener(this);
        help.setOnClickListener(this);
        userDatabaseHelper = new UserDatabaseHelper(this);

        if (!userDatabaseHelper.checkUser(userid)) {
          freetrial.setVisibility(View.GONE);
          loanpayment.setVisibility(View.GONE);
        }


        //User profile



        listView = findViewById(R.id.list_item);
        users = userDatabaseHelper.getdata(userid.toString().trim());
        arrayAdapter = new ArrayAdapter<User>(this,android.R.layout.simple_expandable_list_item_1, users){   @Override
        public View getView(int position, View convertView, ViewGroup parent){
            // Cast the list view each item as text view
            TextView item = (TextView) super.getView(position,convertView,parent);

            // Set the typeface/font for the current item

            // Set the list view item's text color
            item.setTextColor(Color.parseColor("#21CEC9"));

            // item.setTypeface(item.getTypeface(), Typeface.BOLD);

            item.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);

            item.setGravity(Gravity.CENTER);

            return item;
        }
        };
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = users.get(position);

                Intent intent = new Intent(CustomerMenu.this, UserUpdateActivity.class);
                intent.putExtra("USER", user);
                startActivity(intent);

            }
        });
            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.buyjazakit:
                        buyJazakit();
                        break;
                    case R.id.usbcharge:
                        usbCharge();
                        break;
                    case R.id.comodity:
                        buyCommodities();
                        break;
                    case R.id.jazapackswap:
                        jazaSwap();
                        break;
                    case R.id.freetrial:
                        freetrialDailog();
                        break;
                    case R.id.membership:
                        membership();
                        break;
                    case R.id.exchange:
                        addOfferings();
                        break;
                    case R.id.loanpayment:
                        payloan();
                        break;
                    case R.id.logout:
                        logoutsharedpreference();
                        break;
                    case R.id.help:
                        help();
                        break;
                }

            }

            public void buyJazakit() {
                Intent intent = new Intent(this, BuyJazakit.class);
                finishAffinity();
                startActivity(intent);
            }

             public void addOfferings() {
                Intent intent = new Intent(this, AddOfferings.class);
                finishAffinity();
                startActivity(intent);
            }

            public void payloan(){
                Intent intent = new Intent(this, LoanPayment.class);
                startActivity(intent);

            }

            public void jazaSwap(){
                Intent intent = new Intent(this, JazaPackOfferings.class);
                finishAffinity();
                startActivity(intent);

            }

             public void usbCharge(){
                Intent intent = new Intent(this, UsbCharge.class);
                finishAffinity();
                startActivity(intent);

    }
            public void buyCommodities(){
                Intent intent = new Intent(this, Commodity.class);
                finishAffinity();
                startActivity(intent);

            }


    public void membership(){
        Intent intent = new Intent(this, Membership.class);
        startActivity(intent);
    }


    public void help(){
        Intent intent = new Intent(this, Help.class);
        finishAffinity();
        startActivity(intent);

    }

    public void freetrialDailog(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle("FREE TRIAL");

        // set dialog message
        alertDialogBuilder
                .setMessage("TAKE OUT OR RETURN FREE TRIAL")
                .setCancelable(false)
                .setPositiveButton("TAKE OUT",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent intent = new Intent(activity, StartFreeTrial.class);
                        startActivity(intent);

                    }
                })
                .setNegativeButton("RETURN",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent intent = new Intent(activity, EndFreeTrial.class);
                        startActivity(intent);

                    }
                });


        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logoutsharedpreference() {
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        finishAffinity();
        editor.clear();
        editor.commit();
        finish();

    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, MainActivity.class);
        finishAffinity();
        logoutsharedpreference();
        startActivity(newIntent);
        finish();
    }

    public void launchTestService(){
        Intent i = new Intent(this, PublishMe.class);
        startService(i);
    }
}


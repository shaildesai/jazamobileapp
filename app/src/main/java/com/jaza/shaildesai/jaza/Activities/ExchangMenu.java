package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.Model.Transaction;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class ExchangMenu extends AppCompatActivity implements View.OnClickListener {
    private final  ExchangMenu activity = ExchangMenu.this;
    private TextView Exchange_name;
    private TextView offering_price;
    private Button Exchange_confiorm;
    private UserDatabaseHelper userDatabaseHelper;
    private Offerings offerings;
    private Transaction transaction;
    private  String user_id;
    private String offering_id;
    String event_type_enum = "6";
    public long timestamp;
    public String transactionevent = "trans_mob";
    private String user_balance;
    public StringBuilder csvList;
    public String unique_pub_id;
    public String attendent_id;
    public String offering;
    private String getdata;
    private SharedPreferences sharedpreferencesattendent;
    private Publish publish;
    private String csv;
    private String SEPERATOR =",";
    private String enum_type = "8";
    private String deviceid;
    private Helper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchang_menu);

        initViews();
        initObjects();
        getValues();
        sharedpreferencesattendent= this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        attendent_id = sharedpreferencesattendent.getString(Attendent, "");

    }

    private void initViews() {
        Exchange_name= findViewById(R.id.exchangeitemname);
         Exchange_confiorm= findViewById(R.id.exchange);
    }

    private void initObjects(){
        userDatabaseHelper = new UserDatabaseHelper(this);
        offerings = new Offerings();
        transaction = new Transaction();
        Exchange_confiorm.setOnClickListener(this);
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        user_id = sharedpreferences.getString(Name, "");
        deviceid = sharedpreferences.getString(IMEI,"");
        publish = new Publish();
        helper = new Helper(this);

    }

    private void getValues(){
        offerings = (Offerings) getIntent().getExtras().getSerializable("EXCHANGE");
        Exchange_name.setText(offerings.getOffering_name());
        offering_id = offerings.getOffering_id();
        user_balance = userDatabaseHelper.userBalance(user_id);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirmpayment:
                confirmationDailog();
                break;

        }
    }

    public void confirmationDailog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(R.string.exchange_conformation);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.are_you_sure_you_want_to_exchange)
                .setCancelable(false)
                .setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        storeTransaction();


                    }
                })
                .setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private void storeTransaction(){

        transaction.setUser_Id(user_id);
        transaction.setTrans_type(transactionevent);
        transaction.setOffering_id(offering_id);
        transaction.setPayment_amount(offerings.getPrice());
        transaction.setAttendent_id(attendent_id);


        userDatabaseHelper.transactionJazakit(transaction);
        Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
        user_balance = userDatabaseHelper.userBalance(user_id);
        helper.storetranspublish(createCommodityString(),deviceid);
        helper.updateInventoryDecrease(offering_id);
        logoutConformation();

    }

    public String createCommodityString() {

        List<String> dataList = new ArrayList<>();

        int pub_id = userDatabaseHelper.lastpub();
        String offeringversion = userDatabaseHelper.getoffering(offering_id);
        timestamp = System.currentTimeMillis() / 1000L;
        String usersid = user_id;
        String transevent = transactionevent;
        String amount = offering_price.getEditableText().toString();

        dataList.add(String.valueOf(pub_id));
        dataList.add(enum_type);
        dataList.add(String.valueOf(timestamp));
        dataList.add(offeringversion);
        dataList.add(usersid);
        dataList.add(offering_id);
        dataList.add(amount);
        dataList.add(userDatabaseHelper.userBalance(user_id));
        dataList.add(attendent_id);

        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;

    }

    public void logoutConformation(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(R.string.contin);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.doyouwanttodoanothertrasacion)
                .setCancelable(false)
                .setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent newIntent = new Intent(activity, CustomerMenu.class);
                        startActivity(newIntent);
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        logoutsharedpreference();
                        //bye bye
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logoutsharedpreference() {
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        finish();
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivity(newIntent);
    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, Exchange.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }
}


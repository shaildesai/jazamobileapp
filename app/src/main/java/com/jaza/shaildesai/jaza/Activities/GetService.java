package com.jaza.shaildesai.jaza.Activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.IBinder;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.jaza.shaildesai.jaza.Model.Inventory;
import com.jaza.shaildesai.jaza.Model.Inventory_log;
import com.jaza.shaildesai.jaza.Model.Offering_item;
import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.Model.Remittance;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

import static com.jaza.shaildesai.jaza.R.string.error_network_timeout;
import static com.jaza.shaildesai.jaza.R.string.offering;

public class GetService extends Service {

    private UserDatabaseHelper userDatabaseHelper;
    private Publish publish;
    public RequestQueue queue;
    private RequestQueue mRequestQueue;
    private String mStatusCode;
    private NetworkResponse networkResponse;
    private String listener;
    private JSONObject jsonObject;
    private int count;
    public static final int notify = 150000;
    private Timer mTimer = null;
    private Handler mHandler = new Handler();

   // private String url = "https://webhook.site/be3935e3-f6e2-4b19-bd22-7a37b035306f";
  //  private String mJSONURLString = "https://webhook.site/6ae14e55-81e6-4887-b45b-2243df10fc68";
    private long timestamp;
    private RequestQueue requestQueue;
    private String inventorylogurl = "";
    //private String remittance_url = "https://webhook.site/b299b903-2ee5-4060-99e1-db38f11afe5f";
    private Timer timer = new Timer();
    private Inventory_log inventory_log;
    private Remittance remittance;
    private Offerings offeringss;
    private Offering_item offering_item;
    private Inventory inventory;
    private String offeringsurl = "https://webhook.site/3af1d1d7-d3c5-48f9-90fe-13232eac7adf";
    private String missingpuburl= "https://webhook.site/fd5bfcd5-e61b-4834-8f50-af69a2529672";
    private String postmissingpuburl = "http://staging.jazaenergy.com/webhooks/log";
    String publishes_missing_at_hub= "0";
    String publishes_missing;
    private int pm ;
    private int increment =0;
    private String device_id;
    private URL url;
    private String current_offering_id;
    private String max_remittance_id;
    private String max_hub_inventory_log_id;



    public GetService() {
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        userDatabaseHelper = new UserDatabaseHelper(this);
        publish = new Publish();
            device_id = userDatabaseHelper.getImei();


        inventory_log = new Inventory_log();
        remittance = new Remittance();
        offering_item = new Offering_item();
        offeringss = new Offerings();
        inventory = new Inventory();
        current_offering_id = String.valueOf(userDatabaseHelper.maxofferingversionid());
        max_remittance_id = String.valueOf(userDatabaseHelper.lastRemittanceId());
        max_hub_inventory_log_id = String.valueOf(userDatabaseHelper.lastInventoryLogid());
        try {

            try {
                execute();
            } catch (MalformedURLException e) {
                Log.i("I hit malform","not valid url");
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        stopSelf();

        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }



    /*@Override
    public void onCreate() {
        super.onCreate();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    execute();   //Your code here
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        }, 0,  5 * 60 * 1000);//5 Minutes
    }
*/

    @Override
    public void onDestroy() {
        // I want to restart this service again in one hour
        AlarmManager alarm = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarm.set(
                alarm.RTC_WAKEUP,
                System.currentTimeMillis() + (1000 * 60 * 5),
                PendingIntent.getService(this, 0, new Intent(this, GetService.class), 0)

        ); Log.i("I am here in serice", "will run now");
    }


    /*public void initializeTimerTask() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {

                for (int i = 0; i < 100; i++) {
                    Log.i("hey", "I will execute the method");
                }
                try {
                    execute();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }


        }, 10000);

        // If we get killed, after returning from here, restart
    }
   */
    public void execute() throws JSONException, MalformedURLException {
        String finalurl = "";

try {
    URL getVariablesUrl = new URL("http://staging.jazaenergy.com/hubapp_get/get_variables/" + device_id);


    finalurl = String.valueOf(getVariablesUrl);
    Log.i("url is", finalurl);
}
catch (MalformedURLException e){

}
        requestQueue = Volley.newRequestQueue(this);
        jsonObject = getJsonObjectvariables();
        // Initialize a new JsonObjectRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, finalurl, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Do something with response
                //mTextView.setText(response.toString());

                // Process the JSON
                try {

                    String max_hub_remittance = String.valueOf(response.get("max_hub_remittance_id"));
                    String current_offerings_version = response.getString("current_offering_version_id");
                    String max_inventory = response.getString("max_inventory_log_id");
                    publishes_missing = response.getString("publishes_missing");
                    if(publishes_missing==null || publishes_missing!="0"){
                        publishes_missing_at_hub = "0";
                    }



                    pm = Integer.parseInt(publishes_missing);


                    if ((!String.valueOf(userDatabaseHelper.lastRemittanceId()).equals(max_hub_remittance) || max_hub_remittance == null)) {
                        Log.i("max_hub_remittance_id", max_hub_remittance);
                        Log.i("Current_offering_version", current_offerings_version);
                        executegetremittance();
                    } else {
                    }

                    if ((!String.valueOf(userDatabaseHelper.maxofferingversionid()).equals(current_offerings_version)) || current_offerings_version ==null) {
                        Log.i("Current_offering_version", current_offerings_version);

                        executeStoreOfferings();
                    } else {
                    }

                    if ((!String.valueOf(userDatabaseHelper.lastInventoryLogid()).equals(max_inventory))|| max_inventory == null) {
                        Log.i("max_hub_inventory", max_inventory);
                        Log.i("I am here" , "Gonna start publish");
                        executegetinventory();
                    } else {
                    }

                    try {
                         if ((!(publishes_missing_at_hub).equals(publishes_missing)) || publishes_missing ==null)  {
                             Log.i("publishes_missing", publishes_missing);
                             executeGetpub();
                             publishes_missing_at_hub = publishes_missing;

                         }
                     else {
                    }
                }    catch (Exception e){
                        Log.i("error in publishes_missing", "error");

                    }


                    Log.i("current_offering_version_id", current_offerings_version);
                    Log.i("max_inventory_log_id", max_inventory);
                    Log.i("offerin_max_id", String.valueOf(userDatabaseHelper.maxofferingversionid()));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Display the formatted json data in text view

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Do something when error occurred
                for (int i = 0; i <= 100; i++) {
                    Log.i("error", "error");
                }

            }
            // Add JsonObjectRequest to the RequestQueue

        });
        requestQueue.add(jsonObjectRequest);
    }


    public JSONObject getJsonObjectvariables() throws JSONException {
        JSONObject publishList = new JSONObject();
        String x = null;
        timestamp = System.currentTimeMillis() / 1000L;


        publishList.put("event", "get_variables");
        publishList.put("data", userDatabaseHelper.lastpub());
        publishList.put("device_id", "1234567");
        publishList.put("publish_at", String.valueOf(timestamp));
        publishList.put("app_version", 1);

        // publishList.put("published_at","xyz");
        return publishList;
    }

    private void executegetinventory() {

        requestQueue = Volley.newRequestQueue(this);

        String inventory_url = "";

        try {
            URL getVariablesUrl = new URL("http://staging.jazaenergy.com/hubapp_get/get_inventory/" + device_id + "/" + max_hub_inventory_log_id);


            inventory_url = String.valueOf(getVariablesUrl);
            Log.i("url is", inventory_url );
        }
        catch (MalformedURLException e){

        }
        // Initialize a new JsonObjectRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, inventorylogurl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Do something with response
                //mTextView.setText(response.toString());

                // Process the JSON
                try {
                    // Get the JSON array
                    JSONArray array = response.getJSONArray("inventory_log");

                    // Loop through the array elements
                    for (int i = 0; i < array.length(); i++) {
                        // Get current json object
                        JSONObject inventory = array.getJSONObject(i);


                        // Get the current student (json object) data
                        int cloud_log = Integer.parseInt(inventory.getString("cloud_log_id"));


                        int inventory_id = Integer.parseInt(inventory.getString("inventory_id"));
                        String name = inventory.getString("name");
                        int delta = Integer.parseInt(inventory.getString("delta"));
                        if(!userDatabaseHelper.checkInventoryInventoryid(inventory_id)){
                            storeInventory(inventory_id,name);
                        }

                        if (!userDatabaseHelper.checkInventory_id(String.valueOf(cloud_log))) {
                            storeinventorylog(cloud_log, inventory_id, delta);

                            Log.i("cloud_id_is", String.valueOf(cloud_log));
                            Log.i("incentory_is_is", String.valueOf(inventory_id));
                            Log.i("name", name);
                            Log.i("delta", String.valueOf(delta));

                            // Display the formatted json data in text view

                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Do something when error occurred
                for (int i = 0; i <= 100; i++) {
                    Log.i("error in execute inventory", "error");
                }

            }
            // Add JsonObjectRequest to the RequestQueue

        });
        requestQueue.add(jsonObjectRequest);
    }


    private void executegetremittance() {
        requestQueue = Volley.newRequestQueue(this);


        String remittance_url = "";

        try {
            URL getVariablesUrl = new URL("http://staging.jazaenergy.com/hubapp_get/get_remittance/" + device_id + "/" + max_remittance_id);


            remittance_url = String.valueOf(getVariablesUrl);
            Log.i("url is", remittance_url);
        }
        catch (MalformedURLException e){

        }
        // Initialize a new JsonObjectRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, remittance_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Do something with response
                //mTextView.setText(response.toString());

                // Process the JSON
                try {
                    // Get the JSON array
                    JSONArray array = response.getJSONArray("remittance");

                    // Loop through the array elements
                    for (int i = 0; i < array.length(); i++) {
                        // Get current json object
                        JSONObject inventory = array.getJSONObject(i);


                        // Get the current student (json object) data
                        int id = Integer.parseInt(inventory.getString("id"));
                        String type = inventory.getString("type");
                        String date = inventory.getString("date");
                        String amount = inventory.getString("amount");
                        String sent_to = inventory.getString("sent_to");

                        if (!userDatabaseHelper.checkRemittanceId(String.valueOf(id))) {
                            storeremittance(id, type, amount, sent_to);
                            // storeinventorylog(cloud_log, inventory_id, delta);

                            Log.i("cloud_id_is", String.valueOf(id));
                            Log.i("incentory_is_is", String.valueOf(type));
                            Log.i("name", String.valueOf(amount));
                            Log.i("delta", String.valueOf(sent_to));

                            // Display the formatted json data in text view
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Do something when error occurred
                for (int i = 0; i <= 100; i++) {
                    Log.i("error in execute remittance", "error");
                }

            }
            // Add JsonObjectRequest to the RequestQueue

        });
        requestQueue.add(jsonObjectRequest);
    }

    private void executeGetpub() {

        requestQueue = Volley.newRequestQueue(this);

        String executeGetpub_url = "";

        try {
            URL getpub_url = new URL("http://staging.jazaenergy.com/hubapp_get/get_publishes/" + device_id + "/" + publishes_missing_at_hub);


            executeGetpub_url = String.valueOf(getpub_url);
            Log.i("url is", executeGetpub_url );
        }
        catch (MalformedURLException e){

        }
        // Initialize a new JsonObjectRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, executeGetpub_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Do something with response
                //mTextView.setText(response.toString());

                // Process the JSON
                try {
                    // Get the JSON array
                    JSONArray array = response.getJSONArray("missing_publishes");

                    for (int i= 0;i<=array.length();i++)
                    // Loop through the array elements
                    {
                            int startvalue = array.getInt(0);
                            int end_value = array.getInt(1);
                            int id = startvalue;
                            if(startvalue <= end_value) {
                                executemissingpubpost(id);
                                Log.i("The value of id is ", String.valueOf(id));
                                id++;
                            }
                        }
                        stopSelf();
                    } catch (JSONException e1) {
                    e1.printStackTrace();
                }

            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Do something when error occurred
                for (int i = 0; i <= 100; i++) {
                    Log.i("error in getpub ", "error get pub");
                }

            }
            // Add JsonObjectRequest to the RequestQueue

        });
        requestQueue.add(jsonObjectRequest);
    }

    private void executeStoreOfferings() {
        requestQueue = Volley.newRequestQueue(this);
        String get_offering_url = "";

        try {
            URL getofferingurl = new URL("http://staging.jazaenergy.com/hubapp_get/get_offerings/" + device_id );


            get_offering_url = String.valueOf(getofferingurl);
            Log.i("url is", get_offering_url );
        }
        catch (MalformedURLException e){

        }

        // Initialize a new JsonObjectRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, get_offering_url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Do something with response
                //mTextView.setText(response.toString());

                // Process the JSON
                try {
                    // Get the JSON array
                    userDatabaseHelper.deleteInventory();
                    userDatabaseHelper.deleteOfferings();
                    JSONArray array = response.getJSONArray("offering_update");

                    // Loop through the array elements
                    for (int i = 0; i < array.length(); i++) {
                        // Get current json object
                        JSONObject inventory = array.getJSONObject(i);
                        int offering_version_id = Integer.parseInt(inventory.getString("offering_version_id"));
                        // Get the current student (json object) data
                        JSONObject offering = inventory.getJSONObject("offering");
                       // JSONArray offering = (inventory.getJSONArray("offering"));
                        {
                            for (int j = 0; j < offering.length(); j++) {


                                String id = offering.getString("id");
                                if (!userDatabaseHelper.checkoffering_id(id)) {
                                    String name = offering.getString("name");
                                    String type = offering.getString("type");
                                    String price = offering.getString("price");
                                    String min_downpayment = offering.getString("min_down");
                                    String duration = offering.getString("duration");
                                    storeOfferings(id, type, name, price, min_downpayment, duration, offering_version_id);


                                    Log.i("id", String.valueOf(id));
                                    Log.i("name", String.valueOf(name));
                                    Log.i("price", String.valueOf(price));
                                    Log.i("type", String.valueOf(type));
                                    Log.i("price", String.valueOf(min_downpayment));
                                    Log.i("duration", String.valueOf(duration));
                                    Log.i("offering_version id ", String.valueOf(offering_version_id));


                                    {
                                        JSONArray offering_items = offering.getJSONArray("offering_items");
                                        {
                                            for (int k = 0; k < offering_items.length(); k++) {

                                                JSONObject offering_item = offering_items.getJSONObject(k);
                                                int item_id = Integer.parseInt(offering_item.getString("inventory_id"));
                                                int item_amount = Integer.parseInt(offering_item.getString("amount"));
                                                int off_id = Integer.parseInt(id);

                                                storeOfferingItems(id, item_id, item_amount);
                                                Log.i("item_id", String.valueOf(item_id));
                                                Log.i("item_amount", String.valueOf(item_amount));

                                            }
                                        }
                                    }
                                }
                            }
                        }


                        // Display the formatted json data in text view
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Do something when error occurred
                for (int i = 0; i <= 100; i++) {
                    Log.i("error in execte store offerings", "error");
                }

            }
            // Add JsonObjectRequest to the RequestQueue

        });
        requestQueue.add(jsonObjectRequest);
    }






    private void storeinventorylog(int cloud, int id, int delta) {

        inventory_log.setCloud_log_id(cloud);
        inventory_log.setInventory_id(id);
        inventory_log.setDelta(delta);

        userDatabaseHelper.addInventorylog(inventory_log);


    }

    private void storeremittance(int id, String type, String amount, String sent_to) {

        remittance.setRemiitance_id(id);
        remittance.setRemittance_type(type);
        remittance.setAmount(amount);
        //remittance.setDate(date);
        remittance.setSent_to(sent_to);

        userDatabaseHelper.addRemittance(remittance);

    }

    private void storeOfferings(String id, String type, String name, String price,String min_downpay,String duration,int offering_v_id) {
        offeringss.setOffering_id(id);
        offeringss.setOffering_type(type);
        offeringss.setOffering_name(name);
        offeringss.setPrice(price);
        offeringss.setMin_down_payment(min_downpay);
        offeringss.setDuration(duration);
        offeringss.setOffering_version_id(offering_v_id);
        userDatabaseHelper.addofferings(offeringss);
    }

    private void storeOfferingItems(String id, int inventory_id, int amount){
        offering_item.setOffering_item_offering_id(id);
        offering_item.setOffering_item_Inventory_id(inventory_id);
        offering_item.setOffering_item_amount(amount);
        userDatabaseHelper.addOffering_item(offering_item);
    }

    private void storeInventory(int id, String name){
        inventory.setInventory_id(id);
        inventory.setInventory_name(name);
        userDatabaseHelper.addInventory(inventory);
    }

    private void executemissingpubpost(int pub_id) throws JSONException {



        JSONObject missjsonpub = userDatabaseHelper.getSpecificPublish(pub_id);


        requestQueue = Volley.newRequestQueue(this);
        // Initialize a new JsonObjectRequest instance
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, postmissingpuburl, missjsonpub, new Response.Listener<JSONObject>() {


            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.equals(null)) {

                            Log.i("print", String.valueOf(response));
                    } else {

                    }
                }
                catch (Exception e){

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Do something when error occurred
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Log.i("error of timeout", String.valueOf(R.string.error_network_timeout));
                   // Toast.makeText(this, R.string.error_network_timeout, Toast.LENGTH_LONG).show();
                } else if (error instanceof AuthFailureError) {
                    Log.i("error of authentication", "No authentication");
                } else if (error instanceof ServerError) {
                    Log.i("error of server", "erver is not responding");
                    //TODO
                } else if (error instanceof NetworkError) {
                    Log.i("error of network", "pure network error");
                    //TODO
                } //else if (error instanceof ParseError) {
                   // Log.i("Parsing error"," unable to parse");
                    ////TODO
               // }






            }
            // Add JsonObjectRequest to the RequestQueue

        });
        requestQueue.add(jsonObjectRequest);
    }



}





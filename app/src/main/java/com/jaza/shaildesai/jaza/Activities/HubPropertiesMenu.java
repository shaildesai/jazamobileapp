package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Model.HubProperties;
import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class HubPropertiesMenu extends AppCompatActivity {

    private TextView hub_imei;
    private TextView cash_at_hub;
    private TextView last_remittance;
    private TextView last_remittance_date;
    private TextView num_users;
    private TextView num_packs;
    private TextView num_publishes;
    private TextView num_publish_logs;
    private TextView packs_out;
    private TextView packs_in;
    private TextView offering_version_id;
    private TextView max_remittance;
    private TextView total_cash;
    private UserDatabaseHelper userDatabaseHelper;
    private PublishMe publishMe;
    private String user_id;
    private String deviceid;
    private SharedPreferences sharedpreferencesattendent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hub_properties_menu);
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        user_id = sharedpreferences.getString(Name, "");
        sharedpreferencesattendent = this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        deviceid= sharedpreferencesattendent.getString(IMEI, "");
        initViews();
        initObjects();
        populatevalues();
        updateHubProperties();
    }

    private  void initViews(){
        hub_imei = findViewById(R.id.hub_id);
        cash_at_hub = findViewById(R.id.cash_at_hub);
        last_remittance = findViewById(R.id.last_remittance_amount);
        last_remittance_date = findViewById(R.id.last_remittance_date);
        total_cash = findViewById(R.id.total_cash_in);
        num_users = findViewById(R.id.num_of_users);
        num_packs = findViewById(R.id.number_of_packs);
        num_publishes = findViewById(R.id.number_of_publishes);
        num_publish_logs = findViewById(R.id.number_of_publishes_logs);
        packs_in = findViewById(R.id.totla_packs_in);
        packs_out = findViewById(R.id.totla_packs_out);
        offering_version_id = findViewById(R.id.offering_version_id);
        max_remittance = findViewById(R.id.max_remittance_id);

    }
    private void initObjects(){
        userDatabaseHelper = new UserDatabaseHelper(this);

    }

    private void populatevalues(){
       int cash = Integer.parseInt(userDatabaseHelper.lastCashAtHub(deviceid));
        int lastremitanceamount = Integer.parseInt(userDatabaseHelper.lastRemittanceAtaHub());
        hub_imei.setText(deviceid);



        last_remittance.setText(String.valueOf(lastremitanceamount));
        last_remittance_date.setText(userDatabaseHelper.lastRemittanceDate());
        total_cash.setText(userDatabaseHelper.totalCashIn(deviceid));
        num_users.setText(String.valueOf(userDatabaseHelper.countUsers()));
        num_packs.setText(String.valueOf(userDatabaseHelper.countPacks()));
        num_publishes.setText(String.valueOf(userDatabaseHelper.countPublishes()));
        num_publish_logs.setText(String.valueOf(userDatabaseHelper.countPublishesLogs()));

        packs_out.setText(String.valueOf(userDatabaseHelper.packsOutCount("2")));
        packs_in.setText(String.valueOf(userDatabaseHelper.packsInCount("6")));
        offering_version_id.setText("0");
        max_remittance.setText("1");

        cash_at_hub.setText(String.valueOf(cash - lastremitanceamount));
    }

    public void updateHubProperties(){


        long cash = Long.parseLong(cash_at_hub.getText().toString());
            String lastremittanceamount = last_remittance.getText().toString();
            String lastremittancedate = last_remittance_date.getText().toString();
            String numusers= num_users.getText().toString();
            String numpacks= num_packs.getText().toString();
            String numpublish= num_publishes.getText().toString();
            String numpublishlogs= num_publish_logs.getText().toString();
            String numouts= packs_out.getText().toString();
            String numin= packs_in.getText().toString();
            String offeringversionid= offering_version_id.getText().toString();
            String maxremittanceid= max_remittance.getText().toString();
            long totalcash = Long.parseLong(total_cash.getText().toString());



       HubProperties hubProperties = new HubProperties(deviceid,cash,lastremittanceamount,lastremittancedate,numusers,numpacks,numpublish,numpublishlogs,numouts,numin,offeringversionid,maxremittanceid,totalcash);

            int result = userDatabaseHelper.updateHubPropertiesValue(hubProperties);

            if(result > 0) {
                Toast.makeText(this, "succeusflly inserted", Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(this,"Not inserted",Toast.LENGTH_SHORT).show();
            }


    }




}

package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class HubWallet extends AppCompatActivity {
    private TextView cash_at_hub;
    private TextView last_remittance;
    private TextView last_remittance_date;
    private TextView total_cash;
    private String user_id;
    private SharedPreferences sharedpreferencesattendent;
    private String deviceid;
    private UserDatabaseHelper userDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hub_wallet);
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        user_id = sharedpreferences.getString(Name, "");
        sharedpreferencesattendent = this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        deviceid= sharedpreferencesattendent.getString(IMEI, "");
        initViews();
        initObjects();
        populatevalues();

    }

    private  void initViews(){
        cash_at_hub = findViewById(R.id.cash_at_hub);
        last_remittance = findViewById(R.id.last_remittance_amount);
        last_remittance_date = findViewById(R.id.last_remittance_date);
        total_cash = findViewById(R.id.total_cash_in);
    }
    private void initObjects(){
        userDatabaseHelper = new UserDatabaseHelper(this);

    }

    private void populatevalues(){
        int cash = Integer.parseInt(userDatabaseHelper.lastCashAtHub(deviceid));
        int lastremitanceamount = Integer.parseInt(userDatabaseHelper.lastRemittanceAtaHub());

        last_remittance.setText(String.valueOf(lastremitanceamount));
        last_remittance_date.setText(userDatabaseHelper.lastRemittanceDate());
        total_cash.setText(userDatabaseHelper.totalCashIn(deviceid));
        cash_at_hub.setText(String.valueOf(cash - lastremitanceamount));

    }
}

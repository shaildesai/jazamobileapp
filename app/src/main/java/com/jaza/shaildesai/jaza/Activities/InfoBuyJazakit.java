package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class InfoBuyJazakit extends AppCompatActivity  implements View.OnClickListener {

    public String user_id;
    public TextView offering_name;
    public TextView offering_price;
    public Button paymentconfirmation;
    public Offerings offerings;
    public UserDatabaseHelper offeringDatabaseHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_buy_jazakit);
        offeringDatabaseHelper = new UserDatabaseHelper(this);
        offerings = (Offerings) getIntent().getExtras().getSerializable("BUYKIT");
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);


        user_id = sharedpreferences.getString(Name,"");
        offering_name = findViewById(R.id.name);
        offering_price = findViewById(R.id.price);
        paymentconfirmation = findViewById(R.id.paymentconformation);

        offering_name.setText(offerings.getOffering_name());
        offering_price.setText(offerings.getPrice());

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
        }

    }

}

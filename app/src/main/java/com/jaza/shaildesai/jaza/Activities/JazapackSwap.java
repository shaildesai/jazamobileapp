package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.Model.PackStatus;
import com.jaza.shaildesai.jaza.Model.Transaction;
import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class JazapackSwap extends AppCompatActivity implements View.OnClickListener {

    private JazapackSwap activity = JazapackSwap.this;
    private TextView scan_incomming_pack;
    private TextView scan_outgoing_pack;
    private Button scan_incomming_pack_button;
    private Button scan_outgoing_pack_button;
    private Button confirm_swap;
    public UserDatabaseHelper userDatabaseHelper;
    public Transaction transaction;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private static final int request_code = 1;
    public String getdatain;
    public String getdataout;
    public String incomming_id;
    public String outgoing_id;
    public TextView offering_jaza_pack_name;
    public TextView jaza_pack_price;
    public Offerings offerings;
    public String transactionevent = "trans";
    public String offering_id = "4";
    public String user_id;
    public long current_time;
    private String user_balance;
    private String device_id;
    private String enum_type = "4";
    private Helper helper;
    public String csv;
    public String SEPERATOR = ",";
    public String deviceid;
    private StringBuilder csvList;
    private SharedPreferences sharedpreferencesattendent;
    private String attendent_id;
    private PackStatus packStatus;
    private String amount_price = "0";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jazapack_swap);
        initViews();
        initObjects();
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        offerings = (Offerings) getIntent().getExtras().getSerializable("JAZASWAP");

        user_id = sharedpreferences.getString(Name, "");
        device_id = sharedpreferences.getString("IMEI", "");



        sharedpreferencesattendent = this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        attendent_id = sharedpreferencesattendent.getString(Attendent, "");

        if((userDatabaseHelper.userMembershipDate(user_id)).compareTo(getCurrentTime())<0) {
            jaza_pack_price.setText(amount_price);
        }
        else {

            jaza_pack_price.setText(offerings.getPrice());
        }
        offering_jaza_pack_name.setText(offerings.getOffering_name());

        user_balance = userDatabaseHelper.userBalance(user_id);

    }

    public void initViews() {
        scan_incomming_pack = findViewById(R.id.scanincommingpack);
        scan_outgoing_pack = findViewById(R.id.scanoutgoingpack);
        scan_incomming_pack_button = (Button) findViewById(R.id.scanincommingpackbutton);
        scan_outgoing_pack_button = findViewById(R.id.scanoutgoingpackbutton);
        confirm_swap = findViewById(R.id.confirmswap);
        offering_jaza_pack_name = findViewById(R.id.offeringjazaswapname);
        jaza_pack_price = findViewById(R.id.offeringjazaswapprice);


    }

    public void initObjects() {
        transaction = new Transaction();
        userDatabaseHelper = new UserDatabaseHelper(this);
        offerings = new Offerings();
        scan_incomming_pack_button.setOnClickListener(this);
        scan_outgoing_pack_button.setOnClickListener(this);
        confirm_swap.setOnClickListener(this);
        helper = new Helper(this);
        packStatus = new PackStatus();
    }


    public void storeTransactionswap() {

        transaction.setUser_Id(user_id);
        transaction.setTrans_type(transactionevent);
        transaction.setPack_in_id(scan_incomming_pack.getText().toString());
        transaction.setPack_out_id(scan_outgoing_pack.getText().toString());
        transaction.setOffering_id(offering_id);
        if((userDatabaseHelper.userMembershipDate(user_id)).compareTo(getCurrentTime())<0) {
            transaction.setPayment_amount(amount_price);
        }
        else {

            transaction.setPayment_amount(jaza_pack_price.getText().toString());
        }
        transaction.setPayment_amount(jaza_pack_price.getText().toString());

        userDatabaseHelper.addTransactions(transaction);
        Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
        helper.storetranspublish(createSwapString(), deviceid);
        //helper.updateInventoryDecrease(offering_id);
        if(TextUtils.isEmpty(scan_outgoing_pack.getText())) {
            helper.update_packs(scan_incomming_pack.getText().toString(),user_id,helper.Pack_at_hub,getCurrentTime());
        }
        else if(TextUtils.isEmpty(scan_incomming_pack.getText())) {
            helper.update_packs(scan_outgoing_pack.getText().toString(),user_id,helper.pack_signed_out,getCurrentTime());
        }
        else {
            helper.update_packs(scan_outgoing_pack.getText().toString(),user_id,helper.pack_signed_out,getCurrentTime());
            helper.update_packs(scan_incomming_pack.getText().toString(), user_id, helper.Pack_at_hub, getCurrentTime());
        }

        logoutConformation();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scanincommingpackbutton:
                Intent i = new Intent(this, ScanDataMatrix.class);
                startActivityForResult(i, SECOND_ACTIVITY_REQUEST_CODE);
                break;
            case R.id.scanoutgoingpackbutton:
                Intent o = new Intent(this, ScanDataMatrix.class);
                startActivityForResult(o, request_code);
                break;
            case R.id.confirmswap:

                loanDailog();
                break;

        }
    }

    @Override
    public void onActivityResult(int loginCode, int resultCode, Intent i) {
        switch (loginCode) {
            case 0:
                if (SECOND_ACTIVITY_REQUEST_CODE == loginCode) {
                    if (resultCode == RESULT_OK) {
                        getdatain = i.getStringExtra("scanned_id");
                        scan_incomming_pack.setText(getdatain);
                    }
                }
                break;
            case 1:
                if (request_code == loginCode) {
                    if (resultCode == RESULT_OK) {
                        getdataout = i.getStringExtra("scanned_id");
                        scan_outgoing_pack.setText(getdataout);
                    }
                }
                break;
        }
    }

    public String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd/MM/yyyy");
        String strDate = mdformat.format(calendar.getTime());
        return strDate;
    }




    public String createSwapString() {
        List<String> dataList = new ArrayList<>();
        String amount="0";


        String pubs_id = String.valueOf(userDatabaseHelper.lastpub());
        String usersid = user_id;
        String transevent = transactionevent;
        String ubalance = user_balance;
        String pinid = getdatain;
        String poutid = getdataout;
        if((userDatabaseHelper.userMembershipDate(user_id)).compareTo(getCurrentTime())<0)  {
            amount = "0";
        }
        else {
            amount = jaza_pack_price.getEditableText().toString();

        }
        long timestamp = System.currentTimeMillis() / 1000L;

        dataList.add(pubs_id);
        dataList.add(enum_type);
        dataList.add(String.valueOf(timestamp));
        dataList.add(userDatabaseHelper.getoffering(offering_id));
        dataList.add(usersid);
        dataList.add(pinid);
        dataList.add(poutid);
        dataList.add(amount);
        dataList.add(ubalance);
        dataList.add(attendent_id);


        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;

    }

    public void loanDailog() {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Your Title");

        // set dialog message
        alertDialogBuilder.setMessage("Are you sure you want do swap with packs").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                storeTransactionswap();


            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logoutConformation() {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(R.string.contin);

        // set dialog message
        alertDialogBuilder.setMessage(R.string.doyouwanttodoanothertrasacion).setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent newIntent = new Intent(activity, CustomerMenu.class);
                startActivity(newIntent);
                dialog.cancel();
                finish();
            }
        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                logoutsharedpreference();
                //bye bye
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logoutsharedpreference() {
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        finish();
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivity(newIntent);
    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, CustomerMenu.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }
}

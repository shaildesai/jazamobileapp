package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.HubProperties;
import com.jaza.shaildesai.jaza.Model.Transaction;
import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class LoanPayment extends AppCompatActivity  implements View.OnClickListener {

    private LoanPayment activity = LoanPayment.this;
    private TextView loan_balance;
    private TextView minimum_downpayment_paid;
    private EditText payment_amount;
    private TextView new_balance;
    private Button confirm_payment;
    private UserDatabaseHelper userDatabaseHelper;
    private Transaction transaction;
    public String transactionevent = "trans_mob";
    public String publish_code = "5";
    private String user_id;
    private String user_balance;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private String getdata;
    private SharedPreferences sharedpreferencesattendent;
    private String attendent_id;
    public long timestamp;
    public String offering_version_id;
    public StringBuilder csvList;
    public String csv;
    private static final String SEPERATOR = ",";
    private Helper helper;
    private String deviceid;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_payment);
        initViews();
        initObjects();
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        user_id = sharedpreferences.getString(Name, "");
        deviceid = sharedpreferences.getString(IMEI,"");
        user_balance = userDatabaseHelper.userBalance(user_id);
        sharedpreferencesattendent = this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        attendent_id = sharedpreferencesattendent.getString(Attendent, "");
        loan_balance.setText(userDatabaseHelper.userBalance(user_id));
        minimum_downpayment_paid.setText(userDatabaseHelper.userDownpayment(user_id));
        new_balance.setText("0");
//        new_balance.setText(Integer.parseInt(loan_balance.getText().toString()) - Integer.parseInt(payment_amount.getText().toString()));


    }

    private void initViews() {
        loan_balance = findViewById(R.id.loanamount);
        minimum_downpayment_paid = findViewById(R.id.mimimumdownpayment);
        payment_amount = findViewById(R.id.paymentamount);
        new_balance = findViewById(R.id.newbalance);
        confirm_payment = findViewById(R.id.confirmpayment);


    }

    private void initObjects() {
        confirm_payment.setOnClickListener(this);
        new_balance.setOnClickListener(this);
        userDatabaseHelper = new UserDatabaseHelper(this);
        transaction = new Transaction();
        helper = new Helper(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirmpayment:
                conformationDailog();

                break;
            case R.id.newbalance:
                 if(TextUtils.isEmpty(payment_amount.getText())) {
                                 Toast.makeText(this, R.string.enteramount, Toast.LENGTH_SHORT).show();
                                 return;
                  }
                  else {
                     new_balance.setText(String.valueOf(Integer.parseInt(loan_balance.getText().toString()) - Integer.parseInt(payment_amount.getText().toString())));
                 }
                break;
        }

    }

    public void conformationDailog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle("Loan payment");

        // set dialog message
        alertDialogBuilder.setMessage("Are you sure you want to Pay amount").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                storeTransaction();
                createLoanPaymentString();

            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void storeTransaction() {

        transaction.setUser_Id(user_id);
        transaction.setTrans_type(transactionevent);
        transaction.setPack_out_id(payment_amount.getText().toString());
        transaction.setAttendent_id(attendent_id);

        userDatabaseHelper.addTransactions(transaction);
        Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
        helper.storetranspublish(createLoanPaymentString(),deviceid);
        helper.updateCashAtHub(deviceid,payment_amount.getText().toString());
        updateTheUserBalance();
        logoutConformation();



    }

    public String createLoanPaymentString() {
        List<String> dataList = new ArrayList<>();

        String publishid = String.valueOf(userDatabaseHelper.lastpub());
        String pub_id = publish_code;
        timestamp = System.currentTimeMillis() / 1000L;

        String usersid = user_id;
        String paymentamount = payment_amount.getText().toString();
        String userbalance = new_balance.getText().toString();
        String attendentid = attendent_id;

        dataList.add(publishid);
        dataList.add(pub_id);
        dataList.add(String.valueOf(timestamp));
        dataList.add(paymentamount);
        dataList.add(userbalance);
        dataList.add(attendentid);


        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;


    }


    public void updateTheUserBalance() {
        String balance = new_balance.getText().toString();
        String lastpayment = payment_amount.getText().toString();
        User user = new User(user_id, balance, lastpayment);

        int result = userDatabaseHelper.updateUserBalance(user);

        if (result > 0) {
            Toast.makeText(this, "User Updated", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "i am not updated", Toast.LENGTH_SHORT).show();
        }


    }





    public void logoutConformation() {


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set title
        alertDialogBuilder.setTitle(R.string.contin);

        // set dialog message
        alertDialogBuilder.setMessage(R.string.doyouwanttodoanothertrasacion).setCancelable(false).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Intent newIntent = new Intent(activity, CustomerMenu.class);
                startActivity(newIntent);
                dialog.cancel();
                finish();
            }
        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                logoutsharedpreference();
                //bye bye
            }
        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logoutsharedpreference() {
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        finish();
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivity(newIntent);
    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, CustomerMenu.class);
        finishAffinity();
        logoutsharedpreference();
        startActivity(newIntent);
        finish();
    }


}

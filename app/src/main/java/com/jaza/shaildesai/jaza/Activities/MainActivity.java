package com.jaza.shaildesai.jaza.Activities;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Model.Attendent_Session;
import com.jaza.shaildesai.jaza.Model.HubProperties;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;


public class MainActivity extends AppCompatActivity implements ViewStub.OnClickListener {


    private static final int REQUEST_READ_PHONE_STATE = 0;
    public static final String MyPrefences = "myprefs";
    public static final String Name = "nameKey";
    public static final String IMEI = "imeinumber";
    public static final String Attendent = "attendentKey";
    private Button login;
    private TextView registeruser;
    private static final int scan_activity_request = 0;
    public String getresult;
    public UserDatabaseHelper userdatabasehelper;
    SharedPreferences sharedpreferences;
    public String message1;
    public SharedPreferences.Editor editor;
    public String xyz;
    public TextView attendentmenu;
    private static final int scan_attendent_menu = 1;
    SharedPreferences sharedpreferencesattendent;
    public SharedPreferences.Editor editorattendent;
    public static final String AttendentPref = "attendentprefs";
    public String getdata;
    public String LoginKey = "isUserlogin";
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    private TelephonyManager mTelephonyManager;
    public String deviceimeiid;
    public String eventdbase = "dbase_mob";
    public String appversion = "1";
    public Publish publish;
    private String attendent_id;
    private TextView technicianmenu;
    private HubProperties hubProperties;
    private Boolean firstTime = null;
    private PendingIntent pendingIntent;
    private AlarmManager manager;



    /*private String getIMEI(Activity activity) {
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_READ_PHONE_STATE);
        }

        return telephonyManager.getDeviceId();


    }*/



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userdatabasehelper = new UserDatabaseHelper(this);
        //remove after checked
        initobjects();
        initListner();
        launchpublishserivce();

        sharedpreferences= getSharedPreferences("Myprefs", Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        sharedpreferencesattendent = getSharedPreferences(AttendentPref, Context.MODE_PRIVATE);
        editorattendent = sharedpreferencesattendent.edit();
        editorattendent.putBoolean(LoginKey,false);
        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
             getDeviceImei();
            deviceimeiid = getDeviceImei();
            isFirstTime();



        }




    }



    public void initobjects() {

        login = (Button) findViewById(R.id.login_main);
        //registeruser = (TextView) findViewById(R.id.register_main);
        attendentmenu=(TextView)findViewById(R.id.attendent) ;
        technicianmenu = findViewById(R.id.technicianmenu);

    }

    private void initListner() {
        login.setOnClickListener(this);
       // registeruser.setOnClickListener(this);
        attendentmenu.setOnClickListener(this);
        technicianmenu.setOnClickListener(this);

        publish = new Publish();
        hubProperties = new HubProperties();

    }

        // Add extras to the bundle



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login_main:
                sharedpreferencesattendent= this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
                attendent_id = sharedpreferencesattendent.getString(Attendent, "");
                if(attendent_id.equals("")){
                    Toast.makeText(getApplicationContext(), R.string.pleasesigninasattendent, Toast.LENGTH_SHORT).show();
                }
                else {
                    launchomemenu();
                    deviceimeiid = getDeviceImei();
                    storeTransaction();
                }


                break;
           // case R.id.register_main:

                  //  launchregistrationpage();

                //Transaction to register user fragment
               // break;
            case R.id.attendent:
                launchAttendentMenu();
                deviceimeiid = getDeviceImei();//Transaction to register user fragment
                break;
            case R.id.technicianmenu:
                launchtehnicianmenu();

                break;


        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
        }
    }

    public String getDeviceImei() {

        mTelephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the model grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        String deviceid = mTelephonyManager.getDeviceId();

        return deviceid;
    }

    public void launchomemenu() {
        Intent intent = new Intent(getApplicationContext(), ScanDataMatrix.class);
        startActivityForResult(intent, scan_activity_request);

    }

    public void launchtehnicianmenu() {
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);

    }


    public void launchpublishserivce() {
        Intent intent = new Intent(getApplicationContext(), PublishMe.class);
        startService(intent);

    }



    public void launchAttendentMenu() {
        if(sharedpreferencesattendent.getBoolean(LoginKey,true)){
            Intent intent = new Intent(getApplicationContext(), AttendentMenu.class);
            //intent.putExtra("user_id", getdata);
            startActivity(intent);
        }
            else {
            Intent intent = new Intent(getApplicationContext(), ScanDataMatrix.class);
            startActivityForResult(intent, scan_attendent_menu);
        }
    }


    public void onActivityResult(int loginCode, int resultCode, Intent i) {


            switch (loginCode) {
                case 0:
                    if (scan_activity_request == loginCode) {
                        if (resultCode == RESULT_OK) {
                            getresult = i.getStringExtra("scanned_id");
                            // Toast.makeText(getApplicationContext(), "Hey welcome" + getresult, Toast.LENGTH_SHORT).show();
                            //userdatabasehelper.checkUser(getresult);

                                deviceimeiid = getDeviceImei();
                                sharedpreferences = getSharedPreferences(MyPrefences, Context.MODE_PRIVATE);
                                editor = sharedpreferences.edit();
                                editor.putString(Name, getresult);
                                editor.putString(IMEI,deviceimeiid);
                                editor.commit();
                                Intent intent = new Intent(getApplicationContext(), CustomerMenu.class);
                                intent.putExtra("user_id", getresult);
                                startActivity(intent);
                                message1 = sharedpreferences.getString(Name, "");
                                Toast.makeText(getApplicationContext(), R.string.heywelcome + message1, Toast.LENGTH_SHORT).show();
                            } //else {
                            /// Toast.makeText(getApplicationContext(), "Sorry You are not a member sign in as tourist", Toast.LENGTH_SHORT).show();

                            //}


                    }
                    break;
                case 1:
                    if (scan_attendent_menu == loginCode) {
                        if (resultCode == RESULT_OK) {
                            getdata = i.getStringExtra("scanned_id");
                                if (scan_attendent_menu == loginCode) {
                                    if (userdatabasehelper.checkUser(getdata)) {
                                        deviceimeiid = getDeviceImei();
                                        editorattendent.putString(Attendent, getdata);
                                        editorattendent.putString(IMEI,deviceimeiid);
                                        editorattendent.putBoolean(LoginKey,true);
                                        editorattendent.commit();

                                        Intent intent = new Intent(getApplicationContext(), AttendentMenu.class);
                                        intent.putExtra("device_id", getdata);
                                        startActivity(intent);
                                        Toast.makeText(getApplicationContext(), R.string.heywelcome + getdata, Toast.LENGTH_SHORT).show();
                                    }

                                }
                            }
                        }

                    break;
            }



        }

    private void storeTransaction() {
        hubProperties.setHub_imei(deviceimeiid);
        userdatabasehelper.addHubProperties(hubProperties);
        // Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
    }
    private boolean isFirstTime() {
        if (firstTime == null) {
            SharedPreferences mPreferences = this.getSharedPreferences("first_time", Context.MODE_PRIVATE);
            firstTime = mPreferences.getBoolean("firstTime", true);
            if (firstTime) {
                storeTransaction();
                SharedPreferences.Editor editor = mPreferences.edit();
                editor.putBoolean("firstTime", false);
                editor.commit();
            }
        }
        return firstTime;
    }


    /* public void launchregistrationpage() { //when register button is clicked it navigates to register user menu

         findViewById(
                R.id.linear1
         ).setVisibility(View.GONE);
         findViewById(
                 R.id.linear2
         ).setVisibility(View.GONE);
         findViewById(
                 R.id.MyFragment
         ).setVisibility(View.VISIBLE);
         FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();
         RegisterUser regcomplainfragment = new RegisterUser();
         fragmenttransaction.replace(R.id.MyFragment, regcomplainfragment);//.addToBackStack("MainActivity");
         fragmenttransaction.commit();

        Intent intent = new Intent(this, RegisterUser.class);
        deviceimeiid = getDeviceImei();
        intent.putExtra("imei",deviceimeiid);
        startActivity(intent);
}
    */




}





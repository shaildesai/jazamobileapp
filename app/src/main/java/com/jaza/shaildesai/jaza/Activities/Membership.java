package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class Membership extends AppCompatActivity {

    private UserDatabaseHelper offeringDatabaseHelper;
    private String userid;
    public ListView listView;
    List<Offerings> offerings;
    ArrayAdapter<Offerings> arrayAdapter;
    public static final String OFFERING_TYPE = "6";
    public  String transactionevent = "trans";
    public String hub_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership);

        offeringDatabaseHelper = new UserDatabaseHelper(this);
        SharedPreferences sharedpreferences = this.getApplicationContext().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        userid = sharedpreferences.getString(Name,"");

        listView = findViewById(R.id.membershipofferings);
        offerings = offeringDatabaseHelper.getofferingdat(OFFERING_TYPE);
        arrayAdapter = new ArrayAdapter<Offerings>(this,android.R.layout.simple_expandable_list_item_1, offerings){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Cast the list view each item as text view
                TextView item = (TextView) super.getView(position,convertView,parent);

                // Set the typeface/font for the current item

                // Set the list view item's text color
                item.setTextColor(Color.parseColor("#21CEC9"));

                // item.setTypeface(item.getTypeface(), Typeface.BOLD);

                item.setTextSize(TypedValue.COMPLEX_UNIT_DIP,20);

                item.setGravity(Gravity.CENTER);



                // return the view
                return item;
            }
        };

        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Offerings offering = offerings.get(position);

                Intent intent = new Intent(Membership.this, MembershipSubscription.class);

                intent.putExtra("MEMBERSHIP", offering);
                startActivity(intent);


            }
        });

    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, CustomerMenu.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }
}

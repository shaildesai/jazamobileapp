package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.Model.Transaction;
import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class MembershipSubscription extends AppCompatActivity implements View.OnClickListener {


    private final  MembershipSubscription activity = MembershipSubscription.this;
    private TextView membership_type;
    private TextView membership_price;
    private TextView membership_duration;
    private Button confirm_payment;
    private UserDatabaseHelper userDatabaseHelper;
    private Offerings offerings;
    private Transaction transaction;
    private User user;
    private  String user_id;
    private String offering_id;
    String event_type_enum = "8";
    public long timestamp;
    public String transactionevent = "trans_mob";
    private String user_balance;
    public StringBuilder csvList;
    public String unique_pub_id;
    public String attendent_id;
    public String offering;
    private String getdata;
    private SharedPreferences sharedpreferencesattendent;
    private Helper helper;
    private String deviceid;
    private String SEPERATOR = ",";
    private String csv;

    protected void onCreate(Bundle savedInstanceState)  {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_subscription);
        initView();
        initObjects();
        getValues();
        sharedpreferencesattendent= this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        attendent_id = sharedpreferencesattendent.getString(Attendent, "");

    }


    private void initView(){
        membership_type = findViewById(R.id.membershipofferings);
        membership_price = findViewById(R.id.price);
        membership_duration = findViewById((R.id.duration));
        confirm_payment = findViewById(R.id.confirmpayment);

    }
    private void initObjects(){
        userDatabaseHelper = new UserDatabaseHelper(this);
        offerings = new Offerings();
        transaction = new Transaction();
        confirm_payment.setOnClickListener(this);
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        user_id = sharedpreferences.getString(Name, "");
        deviceid = sharedpreferences.getString(IMEI,"");
        user = new User();
        helper = new Helper(this);

    }

    private void getValues(){
        offerings = (Offerings) getIntent().getExtras().getSerializable("MEMBERSHIP");
        membership_type.setText(offerings.getOffering_name());
        membership_duration.setText((offerings.getDuration()));
        membership_price.setText(offerings.getPrice());

        offering_id = offerings.getOffering_id();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmpayment:
                confirmationDailog();
                break;
        }

    }
    public void confirmationDailog(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle("Loan payment");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are you sure you want to Pay amount")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        storeTransaction();
                        storeusermembershipdetails();
                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private void storeTransaction(){

        transaction.setUser_Id(user_id);
        transaction.setTrans_type(transactionevent);
        transaction.setOffering_id(offering_id);
        transaction.setPayment_amount(membership_price.getText().toString());
        transaction.setAttendent_id(attendent_id);


        userDatabaseHelper.transactionJazakit(transaction);
        user_balance = userDatabaseHelper.userBalance(user_id);
        helper.updateCashAtHub(deviceid,membership_price.getText().toString());
        helper.storetranspublish(createMembershipString(),deviceid);
        helper.updateInventoryDecrease(offering_id);
        logoutConformation();

    }

    private  void storeusermembershipdetails(){
        String time;
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, Integer.parseInt(membership_duration.getText().toString()));
        String membershipdate = dateFormat.format(cal.getTime());
        time = dateFormat.format(cal.getTime());

        User user = new User(user_id,membershipdate);

        int result = userDatabaseHelper.updateUserMembershipDate(user);

        if(result > 0){
            Toast.makeText(this, R.string.your_membeship_wil_end + time, Toast.LENGTH_SHORT).show();
        }

        else
        {
            Toast.makeText(this,R.string.sorry_you_can_not_be_member, Toast.LENGTH_SHORT).show();
        }



    }

    public void logoutConformation(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);
        // set title
        alertDialogBuilder.setTitle(R.string.contin);
        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.doyouwanttodoanothertrasacion)
                .setCancelable(false)
                .setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent newIntent = new Intent(activity, CustomerMenu.class);
                        startActivity(newIntent);
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        logoutsharedpreference();
                        //bye bye
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logoutsharedpreference() {
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        finish();
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivity(newIntent);
    }



    public String createMembershipString() {

        List<String> dataList = new ArrayList<>();

        int pub_id = userDatabaseHelper.lastpub();
        String offeringversion = userDatabaseHelper.getoffering(offering_id);
        timestamp = System.currentTimeMillis() / 1000L;
        String usersid = user_id;


        dataList.add(String.valueOf(pub_id));
        dataList.add(event_type_enum);
        dataList.add(String.valueOf(timestamp));
        dataList.add(offeringversion);
        dataList.add(usersid);
        dataList.add(offering_id);
        dataList.add(membership_price.getEditableText().toString());
        dataList.add(userDatabaseHelper.userBalance(user_id));
        dataList.add(attendent_id);

        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;
    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, Membership.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }





}

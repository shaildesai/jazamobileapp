package com.jaza.shaildesai.jaza.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.Model.Transaction;
import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;


public class Payment extends AppCompatActivity implements View.OnClickListener {
    TextView offering_name, offering_price;
    String user_balance;
    String user_id;
    String getpackid;
    public EditText editloan;
    public Button scanpackid;
    public EditText pack_id;
    public TextView user_downpayment;
    Button storepayment;
    UserDatabaseHelper userDatabaseHelper;
    Transaction transaction;
    public String transactionevent = "trans_mob";
    public String offering_id;
    private static final int scan_packid_request = 0;
    public StringBuilder csvList;
    public Offerings offerings;
    public Publish publish;
    public int pub_id;
    public long timestamp;
    public User user;
    public int loan_amount;
    public String balance;
    String loanpublish_id = "2";
    String noloanpublish_id = "3";
    private String payable_amount;
    private String hub_id;
    private MainActivity mainActivity;
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    private TelephonyManager mTelephonyManager;
    private final  Payment activity = Payment.this;
    public String csv;
    public String SEPERATOR = ",";
    public String deviceid;
    private Helper helper;
    private SharedPreferences sharedpreferencesattendent;
    private String attendent_id;
    private ArrayList arrayList;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
         mainActivity = new MainActivity();
        initViews();
        initObjects();
        getValues();

        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            getDeviceImei();
        }
        sharedpreferencesattendent= this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        attendent_id = sharedpreferencesattendent.getString(Attendent, "");



    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_READ_PHONE_STATE && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getDeviceImei();
        }
    }

    public String getDeviceImei() {

        mTelephonyManager = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the model grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        String deviceid = mTelephonyManager.getDeviceId();
        return deviceid;
    }

    public void initObjects() {
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        user_id = sharedpreferences.getString(Name, "");
        deviceid = sharedpreferences.getString(IMEI,"");
        userDatabaseHelper = new UserDatabaseHelper(this);
        storepayment.setOnClickListener(this);
        scanpackid.setOnClickListener(this);
        transaction = new Transaction();
        publish = new Publish();
        user = new User();
        helper = new Helper(this);
        arrayList = new ArrayList();
    }

    public void initViews() {
        // Jazakit Loan
        offerings = (Offerings) getIntent().getExtras().getSerializable("BUYKIT");
        offering_name = findViewById(R.id.offeringname);
        offering_price = findViewById(R.id.offeringprice);
        storepayment = findViewById(R.id.transactionkit);
        pack_id = findViewById(R.id.packoutid);
        user_downpayment = findViewById(R.id.downpayment);
        scanpackid = findViewById(R.id.scanpackout);
        editloan = findViewById(R.id.enterdownpayment);
    }

    public void getValues() {
        offering_name.setText(offerings.getOffering_name());
        offering_price.setText(offerings.getPrice());
        offering_id = offerings.getOffering_id();
        if (!userDatabaseHelper.checkUser(user_id)) {
            Toast.makeText(this,R.string.sorryyoucannottakeloan , Toast.LENGTH_LONG).show();
            logoutsharedpreference();
        }
        else
        {
            if (checkhomekitbuytype()) {
                user_downpayment.setVisibility(View.VISIBLE);
                editloan.setVisibility(View.VISIBLE);
                user_downpayment.setText(offerings.getMin_down_payment());

            }

        }


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.transactionkit:
                loanDailog();
                payable_amount = editloan.getText().toString();
                break;
            case R.id.scanpackout:
                Intent intent = new Intent(getApplicationContext(), ScanDataMatrix.class);
                startActivityForResult(intent, scan_packid_request);
        }

    }

    private boolean checkhomekitbuytype() {
        if (Integer.parseInt(offerings.getMin_down_payment()) != Integer.parseInt(offerings.getPrice())) {
            return true;
        } else {
            return false;
        }
    }

    private void storeTransaction() {

        if(TextUtils.isEmpty(pack_id.getText())) {
            Toast.makeText(this, "please  Scan Pack_id", Toast.LENGTH_SHORT).show();
        }
        else{
            helper.update_packs(pack_id.getText().toString(),user_id,helper.pack_signed_out,getCurrentTime());
        }

        transaction.setUser_Id(user_id);
        transaction.setTrans_type(transactionevent);
        transaction.setPack_out_id(pack_id.getText().toString());
        transaction.setOffering_id(offering_id);
        transaction.setPayment_amount(offering_price.getText().toString());


        userDatabaseHelper.transactionJazakit(transaction);
        helper.storetranspublish(createString(),deviceid);
        helper.updateInventoryDecrease(offering_id);
        Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
        helper.updateCashAtHub(deviceid,offering_price.getText().toString());
        user_balance = userDatabaseHelper.userBalance(user_id);

    }


    public String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = "Current Time : " + mdformat.format(calendar.getTime());
        return strDate;
    }

    private void loanTransaction() {


            transaction.setUser_Id(user_id);
            transaction.setTrans_type(transactionevent);
            transaction.setPack_out_id(pack_id.getText().toString());
            transaction.setOffering_id(offering_id);
            transaction.setPayment_amount(offering_price.getText().toString());



            userDatabaseHelper.transactionJazakit(transaction);
            helper.updateCashAtHub(deviceid,offering_price.getText().toString());
            Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
            user_balance = userDatabaseHelper.userBalance(user_id);
            createLoanString();
        }


    private void updateWithLoan() {

        loan_amount = Integer.parseInt(offerings.getPrice()) - Integer.parseInt(editloan.getText().toString());
        balance = String.valueOf(loan_amount);
        if (!userDatabaseHelper.checkbalance(user_id)) {
            String uloan = String.valueOf(loan_amount);
            String udownpayment = editloan.getText().toString();
            String ubalance = balance;

            user = new User(user_id, uloan, udownpayment, ubalance);

            int result = userDatabaseHelper.updateUserLoan(user);

            if (result > 0) {
                Toast.makeText(this, R.string.user_balance_updated, Toast.LENGTH_SHORT).show();
                loanTransaction();
                helper.storetranspublish(createLoanString(),deviceid);
                helper.updateInventoryDecrease(offering_id);
            } else {
                Toast.makeText(this, R.string.i_am_no_updated, Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, R.string.sorry_you_already_have_a_loan, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int loginCode, int resultCode, Intent i) {
        if (scan_packid_request == loginCode) {
            if (resultCode == RESULT_OK) {
                getpackid = i.getStringExtra("scanned_id");
                pack_id.setText(getpackid);

            }
        }
    }

    public String createString() {
        List<String> dataList = new ArrayList<>();

        int pubid = userDatabaseHelper.lastpub();
        timestamp = System.currentTimeMillis() / 1000L;
        String usersid = user_id;
        String transevent = transactionevent;
        String ubalance = userDatabaseHelper.userLoanAmount(user_id);
        String poutid = getpackid;
        String amount = offerings.getPrice();


        dataList.add(String.valueOf(pubid));
        dataList.add(noloanpublish_id);
        dataList.add(String.valueOf(timestamp));
        dataList.add(userDatabaseHelper.getoffering(offering_id));
        dataList.add(usersid);
        dataList.add(poutid);
        dataList.add(offering_id);
        dataList.add(amount);
        dataList.add(ubalance);
        dataList.add(attendent_id);

        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;
    }


    public String createLoanString() {

        List<String> dataList = new ArrayList<>();

       int pubid = userDatabaseHelper.lastpub();
        timestamp = System.currentTimeMillis() / 1000L;
        String usersid = user_id;
        String transevent = transactionevent;

        String ubalance = String.valueOf(loan_amount);
        String poutid = getpackid;
        String amount = editloan.getEditableText().toString();

        dataList.add(String.valueOf(pubid));
        dataList.add(loanpublish_id);
        dataList.add(String.valueOf(timestamp));
        dataList.add(userDatabaseHelper.getoffering(offering_id));
        dataList.add(usersid);
        dataList.add(poutid);
        dataList.add(offering_id);
        dataList.add(amount);
        dataList.add(ubalance);
        dataList.add(attendent_id);

        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;



    }

    public void logoutConformation(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(R.string.contin);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.doyouwanttodoanothertrasacion)
                .setCancelable(false)
                .setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent newIntent = new Intent(activity, CustomerMenu.class);
                        startActivity(newIntent);
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        logoutsharedpreference();
                        //bye bye
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    public void loanDailog(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(R.string.payment_conformation);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.areyousureyouwanttopay)
                .setCancelable(false)
                .setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        if(checkhomekitbuytype()){
                            updateWithLoan();
                            logoutConformation();

                        }
                        else {
                            storeTransaction();
                            logoutConformation();


                        }

                    }
                })
                .setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        //bye bye
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logoutsharedpreference() {
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        finish();
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivity(newIntent);
    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, BuyJazakit.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }







}




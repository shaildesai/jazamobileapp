package com.jaza.shaildesai.jaza.Activities;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.Model.PublishLog;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Timer;

public class PublishMe extends Service {
    private UserDatabaseHelper userDatabaseHelper;
    private Publish publish;
    public RequestQueue queue;
    private RequestQueue mRequestQueue;
    private String mStatusCode;
    private NetworkResponse networkResponse;
    private String listener;
    private JSONObject jsonObject;
    private int count;
    public static final int notify = 150000;
    private Timer mTimer = null;
    private Handler mHandler = new Handler();
    private  String url = "http://staging.jazaenergy.com/webhooks/log";
    private PublishLog publishLog;

    public PublishMe(Context context) {
        super();

        Log.i("SERVICE", "hService started");
    }


    public PublishMe() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        userDatabaseHelper = new UserDatabaseHelper(this);
        publish = new Publish();
        publishLog = new PublishLog();



      initializeTimerTask();

        return START_STICKY;
    }

    public void initializeTimerTask(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {

                if(userDatabaseHelper.checkPublish()){
                    startandrun();
                }
                else
                {
                    initializeTimerTask();
                }

            }
        }, 10000);

        // If we get killed, after returning from here, restart
    }




    protected void startandrun() {

        initializeTimerTask();
        mRequestQueue = Volley.newRequestQueue(this);
        userDatabaseHelper = new UserDatabaseHelper(this);
        publish = new Publish();

        int countedpub = userDatabaseHelper.countPub();

        if (checkNetworkConnection()) {

                Log.i("Above", "above publish");
            new PublishMe.HTTPAsyncTask().execute(url);

            // This describes what will happen when service is triggered

        } else {

                Log.i("error", "eror");
        }
    }

    public boolean checkNetworkConnection() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        boolean isConnected = false;
        if (networkInfo != null && (isConnected = networkInfo.isConnected())) {
            // show "Connected" & type of network "WIFI or MOBILE"
            //tvisConnected.setText("Connected "+networkInfo.getTypeName());
            // change background color to red
            // tvisConnected.setBackgroundColor(0xFF7CCC26);
            return true;


        } else {
            // show "Not Connected"
            //tvisConnected.setText("Not Connected");
            // change background color to green
            // tvisConnected.setBackgroundColor(0xFFFF0000);
        }

        return isConnected;
    }

    private String httpPost(String myUrl) throws IOException, JSONException {
        String result = " ";

        URL url = new URL(myUrl);

        // 1. create HttpURLConnection
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/json; charset=utf-8");

        // 2. build JSON object
        JSONObject jsonObject = buidJsonObject();

        // 3. add JSON content to POST request body
        setPostRequestContent(conn, jsonObject);


        // 4. make POST request to the given URL
        conn.connect();


        // 5. return response message
        return conn.getResponseMessage() + "";

    }

    public class HTTPAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call: params[0] is the url.
            try {
                try {

                        Log.i("httppost", "abovepost");

                    return httpPost(urls[0]);

                } catch (JSONException e) {
                    e.printStackTrace();
                    return "Error!";
                }
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            mStatusCode = String.valueOf(result);
            if (mStatusCode.equals(String.valueOf("OK"))) {
                if (jsonObject.has("Pub_id")) {
                    int pub_id = 0;
                    try {
                        pub_id = Integer.parseInt(jsonObject.getString("Pub_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    int sucessfult = 1;
                    int i = 0;
                    Publish publish = new Publish(pub_id, sucessfult);
                    count = userDatabaseHelper.countPub();
                        int resultiv = userDatabaseHelper.updatedesuccusful(publish);

                    if (userDatabaseHelper.checkPublishLog(String.valueOf(pub_id))) {
                        userDatabaseHelper.deletepublishlog(String.valueOf(pub_id));
                            Log.i("I am here for deletion","deleye");
                    }

                        if (resultiv > 0) {
                            if (checkNetworkConnection() && i < count - 1) {

                                new PublishMe.HTTPAsyncTask().execute(url);
                                count++;

                                // This describes what will happen when service is triggered

                            }



                        }
                    }

                }
            else{
            try {

                int pub_id = Integer.parseInt(jsonObject.getString("Pub_id"));
                if(!userDatabaseHelper.checkPublishLog(String.valueOf(pub_id))) {// If the pub_id do not recive http Ok response it add that id to publish log
                    publishLog.setPublishbacklogid(String.valueOf(pub_id));
                    userDatabaseHelper.addpulishlog(publishLog);
                }
                } catch (JSONException e1) {
                e1.printStackTrace();

            }

        }
            }

        }

    private JSONObject buidJsonObject() throws JSONException {

        jsonObject = userDatabaseHelper.getAllPublish();
        return jsonObject;
    }

    private void setPostRequestContent(HttpURLConnection conn, JSONObject jsonObject) throws IOException {

        OutputStream os = conn.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(jsonObject.toString());
        Log.i(MainActivity.class.toString(), jsonObject.toString());
        writer.flush();
        writer.close();
        os.close();
    }



}

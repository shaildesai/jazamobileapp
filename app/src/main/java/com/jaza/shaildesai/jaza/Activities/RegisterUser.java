package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.Model.Transaction;
import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.view.ViewStub.*;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;

public class RegisterUser extends AppCompatActivity implements OnClickListener {

    private final RegisterUser activity = RegisterUser.this;
    private EditText user_id;
    private EditText firstname;
    private EditText lastname;
    private EditText phonenumber;
    private EditText birthyear;
    private Spinner gender;
    private EditText attendant;
    private Button register;
    private String getdata;
    private Button scanbutton;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private Transaction transaction;
    private String pub_id;
    private String event_type_enum = "0";
    private String transactionevent = "dbase_mob";
    private SharedPreferences sharedpreferencesattendent;
    private String attendent_id;
    private long timestamp;
    private String csv;
    private String SEPERATOR = ",";
    private StringBuilder csvList;
    private Publish publish;
    private MainActivity mainActivity;
    private String deviceid;
    private int last_pub;
    private Helper helper;


    private UserDatabaseHelper userDatabaseHelper;
    private User user;
    private SQLiteOpenHelper sqLiteHelper;
    private ScanDataMatrix scandatamatrix;
    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 999;
    private TelephonyManager mTelephonyManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_customer);
        getSupportActionBar().hide();

        initViews();
        initObjects();

        // pub_id = String.valueOf((userDatabaseHelper.lastpub()));
        sharedpreferencesattendent = this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        attendent_id = sharedpreferencesattendent.getString(Attendent, "");
        deviceid = sharedpreferencesattendent.getString(IMEI,"");


    }
    //scandatamatrix = (ScanDataMatrix)getActivity();
    //user_id = scandatamatrix.scannedidnumber;

    public void initViews() {
        user_id = (EditText) findViewById(R.id.scanid);
        firstname = (EditText) findViewById(R.id.firstname);
        lastname = (EditText) findViewById(R.id.lastname);
        phonenumber = (EditText) findViewById(R.id.phonenumber);
        birthyear = (EditText) findViewById(R.id.birthyear);
        gender = (Spinner) findViewById(R.id.gender);
        register = (Button) findViewById(R.id.register);
        scanbutton = (Button) findViewById(R.id.scanbutton);

    }


    private void initObjects() {
        userDatabaseHelper = new UserDatabaseHelper(this);
        mainActivity = new MainActivity();
        register.setOnClickListener(this);
        scanbutton.setOnClickListener(this);
        user = new User();
        transaction = new Transaction();
        publish = new Publish();
        helper = new Helper(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register:
                postDataToUserTable();

                break;
            case R.id.scanbutton:
                Intent i = new Intent(this, ScanDataMatrix.class);
                startActivityForResult(i, SECOND_ACTIVITY_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int loginCode, int resultCode, Intent i) {
        if (SECOND_ACTIVITY_REQUEST_CODE == loginCode) {
            if (resultCode == RESULT_OK) {
                getdata = i.getStringExtra("scanned_id");
                user_id.setText(getdata);
            }
        }
    }


    private void postDataToUserTable() {
        if (!userDatabaseHelper.checkUser(user_id.getText().toString().trim())) {

            user.setUser_id(user_id.getText().toString().trim());
            user.setFirst_name(firstname.getText().toString().trim());
            user.setLast_name(lastname.getText().toString().trim());
            user.setPhone_number(phonenumber.getText().toString().trim());
            user.setBirth_year(birthyear.getText().toString().trim());
            user.setGender(String.valueOf(gender.getSelectedItem().toString()));

            if(TextUtils.isEmpty(user_id.getText())) {
                Toast.makeText(this, R.string.enter_user_id, Toast.LENGTH_SHORT).show();
                return;
            }
            else{
                userDatabaseHelper.addUser(user);
                Toast.makeText(this, R.string.userregisteredsucessfully, Toast.LENGTH_SHORT).show();
                helper.storedbasepublish(createRegisterUserString(), deviceid);
                storeTransaction();
                emptyInputEditText();
                registrationConformation();
            }


        } else {
            Toast.makeText(this, "Not inserted", Toast.LENGTH_SHORT).show();
            useralreadyexist();
        }


    }

    private void storeTransaction() {
        transaction.setUser_Id(user_id.getText().toString());
        transaction.setTrans_type(transactionevent);
        transaction.setAttendent_id(attendent_id);

        userDatabaseHelper.transactionJazakit(transaction);
       // Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
    }

    private void emptyInputEditText() {
        user_id.setText(null);
        firstname.setText(null);
        lastname.setText(null);
        phonenumber.setText(null);
        birthyear.setText(null);

    }

    public String createRegisterUserString() {
        List<String> dataList = new ArrayList<>();

        String publishid = String.valueOf(userDatabaseHelper.lastpub());
        String eventtype = event_type_enum;
        timestamp = System.currentTimeMillis() / 1000L;

        String usersid = user_id.getText().toString();
        String fname = firstname.getText().toString();
        String lname = lastname.getText().toString();
        String pnumber = phonenumber.getText().toString();
        String byear = birthyear.getText().toString();
        String gtype = String.valueOf(gender.getSelectedItem().toString());
        String attendentid = attendent_id;


        dataList.add(publishid);
        dataList.add(eventtype);
        dataList.add(String.valueOf(timestamp));
        dataList.add(usersid);
        dataList.add(fname);
        dataList.add(lname);
        dataList.add(pnumber);
        dataList.add(byear);
        dataList.add(gtype);
        dataList.add(attendentid);


        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;

    }



    public void registrationConformation(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(R.string.userregisteredsucessfully);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.Do_you_want_to_register_new_user)
                .setCancelable(false)
                .setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent intent = new Intent(activity,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void useralreadyexist(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(R.string.useralreadyregistered);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.theUserYouWishToRegisterIsPresent)
                .setCancelable(false)
                .setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        emptyInputEditText();
                        dialog.cancel();
                    }
                })
                .setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent intent = new Intent(activity,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, AttendentMenu.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }


}






package com.jaza.shaildesai.jaza.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.widget.Toast;


import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;


public class ScanDataMatrix extends AppCompatActivity {

    public String scannedidnumber;

    public RegisterUser registeruser;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
       // setContentView(R.layout.activity_home_menu);


        registeruser = new RegisterUser();

        final Activity activity = this;
        IntentIntegrator Integrator = new IntentIntegrator(activity);
        Integrator.setDesiredBarcodeFormats(IntentIntegrator.DATA_MATRIX);
        Integrator.setCaptureActivity(CaptureActivityPortrait.class);
        Integrator.setPrompt("scan");
        Integrator.setCameraId(0);
        Integrator.setBeepEnabled(false);
        Integrator.setBarcodeImageEnabled(false);
        Integrator.initiateScan();
    }




        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {

            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result != null) {
                //if qrcode has nothing in it
                if (result.getContents() == null) {
                    onBackPressed();
                    Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, result.getContents(), Toast.LENGTH_LONG).show();
                    scannedidnumber = result.getContents();
                    Intent intent = new Intent();
                    intent.putExtra("scanned_id", scannedidnumber);
                    setResult(RESULT_OK, intent);
                    finish();

                   // Bundle bundle = new Bundle();//create bundle instance
                   // bundle.putString("scanned_id",scannedidnumber);
                    //registeruser.setArguments(bundle);
                    //finish();
                    //registeractivitycall();




                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }

        protected String registeractivitycall(){

            Intent i = new Intent(getApplicationContext(), RegisterUser.class);
            i.putExtra("scanned_id", scannedidnumber);
            startActivity(i);
            finish();


            return scannedidnumber;
        }

    @Override
    public void onBackPressed(){
        super.onBackPressed();

        finish();
    }





    }


package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Inventory;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.Model.Transaction;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class StartFreeTrial extends AppCompatActivity  implements View.OnClickListener {

    private final  StartFreeTrial activity = StartFreeTrial.this;
    private EditText scaned_startfree_trial;
    private Button scan_outgoing_pack;
    private Button confirm_start_free_trial;
    private UserDatabaseHelper userDatabaseHelper;
    private Transaction transaction;
    public String transactionevent = "trans_mob";
    public String offering_id = "5";
    public String publish_code = "0";
    private String user_id;
    private String user_balance;
    private static final int SECOND_ACTIVITY_REQUEST_CODE = 0;
    private String getdata;
    private SharedPreferences sharedpreferencesattendent;
    private String attendent_id;
    private Publish publish;
    private String csv;
    private String SEPERATOR =",";
    private String enum_type = "0";
    private String deviceid;
    private long timestamp;
    private StringBuilder csvList;
    private Helper helper;
    private ArrayList arrayList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_free_trial);
        initViews();
        initObjects();
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        user_id = sharedpreferences.getString(Name, "");
        deviceid = sharedpreferences.getString(IMEI,"");
        user_balance = userDatabaseHelper.userBalance(user_id);
        sharedpreferencesattendent= this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        attendent_id = sharedpreferencesattendent.getString(Attendent, "");




    }

    private void  initViews(){
        scaned_startfree_trial = findViewById(R.id.scannedfreetrialoutpack);
        scan_outgoing_pack = findViewById(R.id.scanfreetrialoutpack);
        confirm_start_free_trial = findViewById(R.id.startfreetrial);


    }

    private void initObjects(){
        scan_outgoing_pack.setOnClickListener(this);
        confirm_start_free_trial.setOnClickListener(this);
        userDatabaseHelper = new UserDatabaseHelper(activity);
        transaction = new Transaction();
        helper = new Helper(activity);
         arrayList = new ArrayList();


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.scanfreetrialoutpack:
                Intent i = new Intent(this, ScanDataMatrix.class);
                startActivityForResult(i, SECOND_ACTIVITY_REQUEST_CODE);
                break;
            case R.id.startfreetrial:
               conformationDailog();
                break;
        }

    }

    @Override
    public void onActivityResult(int loginCode, int resultCode, Intent i) {
        if (SECOND_ACTIVITY_REQUEST_CODE == loginCode) {
            if (resultCode == RESULT_OK) {
                getdata = i.getStringExtra("scanned_id");
                scaned_startfree_trial.setText(getdata);
            }
        }
    }

    public void conformationDailog(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle("Your Title");

        // set dialog message
        alertDialogBuilder
                .setMessage("Are you sure you want to start free trial")
                .setCancelable(false)
                .setPositiveButton("Yes",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        storeTransaction();

                        //storePublish();


                    }
                })
                .setNegativeButton("No",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                });

        // create alert dialog

        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }


    private void storeTransaction(){

        if(TextUtils.isEmpty(scaned_startfree_trial.getText())) {
            Toast.makeText(this, "please  Scan Pack_id", Toast.LENGTH_SHORT).show();
        }
        else{
            helper.update_packs(scaned_startfree_trial.getText().toString(),user_id,helper.pack_signed_out,getCurrentTime());
        }


        transaction.setUser_Id(user_id);
        transaction.setTrans_type(transactionevent);
        transaction.setPack_out_id(scaned_startfree_trial.getText().toString());
        transaction.setOffering_id(offering_id);
        transaction.setAttendent_id(attendent_id);

        userDatabaseHelper.addTransactions(transaction);
        Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
        helper.storetranspublish(createStartFreeTrialString(),deviceid);
        helper.updateInventoryDecrease(offering_id);
        logoutConformation();

    }

    public String getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("HH:mm:ss");
        String strDate = mdformat.format(calendar.getTime());
        return strDate;
    }

    public String createStartFreeTrialString() {

        List<String> dataList = new ArrayList<>();

        int pub_id = userDatabaseHelper.lastpub();
        String offeringversion = userDatabaseHelper.getoffering(offering_id);
        timestamp = System.currentTimeMillis() / 1000L;
        String usersid = user_id;


        dataList.add(String.valueOf(pub_id));
        dataList.add(enum_type);
        dataList.add(String.valueOf(timestamp));
        dataList.add(offeringversion);
        dataList.add(usersid);
        dataList.add(scaned_startfree_trial.getEditableText().toString());
        dataList.add(userDatabaseHelper.userBalance(user_id));
        dataList.add(attendent_id);

        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;
    }


    public void logoutConformation(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(R.string.contin);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.doyouwanttodoanothertrasacion)
                .setCancelable(false)
                .setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent newIntent = new Intent(activity, CustomerMenu.class);
                        startActivity(newIntent);
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        logoutsharedpreference();
                        //bye bye
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logoutsharedpreference() {
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        finish();
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivity(newIntent);
    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, CustomerMenu.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }



/*
        Inventory inventory = new Inventory(inventor_id,newamount);

        int result = userDatabaseHelper.updatedeinventoryamount(inventory);

        if(result > 0){
            Toast.makeText(this,"User Updated", Toast.LENGTH_SHORT).show();
        }

        else
        {
            Toast.makeText(this,"i am not updated", Toast.LENGTH_SHORT).show();
        }
*/


    }






package com.jaza.shaildesai.jaza.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Attendent_Session;
import com.jaza.shaildesai.jaza.Model.Inventory;
import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

public class TechnicianMenu extends AppCompatActivity implements View.OnClickListener {

    private Button attendentmenu;
    private Button gohome;
    private Button getpub;

    private UserDatabaseHelper userdatabasehelper;
    private User user;

    private String getresult;
    SharedPreferences sharedpreferencesattendent;
    public String message1;
    public SharedPreferences.Editor editorattendent;
    public static final String AttendentPref = "attendentprefs";
    public String LoginKey = "isUserlogin";
    public static final String Attendent = "attendentKey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_technician_menu);

        attendentmenu = findViewById(R.id.registerattendent);
        gohome = findViewById(R.id.gohome);
        getpub = findViewById(R.id.getrequest);


        attendentmenu.setOnClickListener(this);
        gohome.setOnClickListener(this);
        getpub.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registerattendent:
                launchattendentregistration();
                finish();
                break;
            case R.id.gohome:
                launcgohome();
                finish();
                break;
            case R.id.getrequest:
                launchTestService();
                finish();
                break;
        }

    }

    private void launchattendentregistration(){
        Intent intent = new Intent(this,AttendentRegistration.class);
        finishAffinity();
        startActivity(intent);
    }


        public void launchTestService(){
            Intent i = new Intent(this, GetService.class);
            startService(i);

    }

        public void launcgohome(){
             Intent i = new Intent(this,MainActivity.class);
            startService(i);
    }
}

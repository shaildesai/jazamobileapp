package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class UsbCharge extends AppCompatActivity {
    private final AppCompatActivity activity = UsbCharge.this;
    private UserDatabaseHelper offeringDatabaseHelper;
    private String userid;
    public ListView listView;
    List<Offerings> offerings;
    ArrayAdapter<Offerings> arrayAdapter;
    public static final String OFFERING_TYPE = "2";
    public  String transactionevent = "trans";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usb_charge);

        offeringDatabaseHelper = new UserDatabaseHelper(this);
        SharedPreferences sharedpreferences = this.getApplicationContext().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        userid = sharedpreferences.getString(Name,"");

        listView = findViewById(R.id.listusbofferings);
        offerings = offeringDatabaseHelper.getofferingdat(OFFERING_TYPE);
        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, offerings);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Offerings offering = offerings.get(position);

                Intent intent = new Intent(UsbCharge.this, UsbPayment.class);

                intent.putExtra("USBCHARGE", offering);
                startActivity(intent);


            }
        });

    }
    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, CustomerMenu.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }


    }


package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.Model.Transaction;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;

public class UsbPayment extends AppCompatActivity implements View.OnClickListener {

    private final UsbPayment activity = UsbPayment.this;
    private TextView offering_name;
    private TextView offering_price;
    private Button confirm_payment;
    private UserDatabaseHelper userDatabaseHelper;
    private Offerings offerings;
    private Transaction transaction;
    private  String user_id;
    private String offering_id;
    String usbchargepublish_id = "6";
    public long timestamp;
    public String transactionevent = "trans_mob";
    private String user_balance;
    public StringBuilder csvList;
    public String unique_pub_id;
    public String attendent_id;
    public String offering;
    private String csv;
    private String SEPERATOR =",";
    private String enum_type = "6";
    private String deviceid;
    private Helper helper;
    private SharedPreferences sharedpreferencesattendent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usb_payment);

        initViews();
        initObjects();
        getValues();

    }

    private void initViews() {
        offering_name = findViewById(R.id.offeringnameusb);
        offering_price = findViewById(R.id.price);
        confirm_payment = findViewById(R.id.confirmpayment);
    }

    private void initObjects(){
        userDatabaseHelper = new UserDatabaseHelper(this);
        offerings = new Offerings();
        transaction = new Transaction();
        confirm_payment.setOnClickListener(this);
        helper = new Helper(this);
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        user_id = sharedpreferences.getString(Name, "");
        deviceid = sharedpreferences.getString(IMEI,"");
        sharedpreferencesattendent= this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        attendent_id = sharedpreferencesattendent.getString(Attendent, "");
    }

    private void getValues(){
        offerings = (Offerings) getIntent().getExtras().getSerializable("USBCHARGE");
        offering_name.setText(offerings.getOffering_name());
        offering_price.setText(offerings.getPrice());
        offering_id = offerings.getOffering_id();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.confirmpayment:
                storeUsbTransaction();

                break;
        }

    }

    private void storeUsbTransaction() {

        transaction.setUser_Id(user_id);
        transaction.setTrans_type(transactionevent);
        transaction.setOffering_id(offering_id);
        transaction.setPayment_amount(offering_price.getText().toString());

        userDatabaseHelper.transactionJazakit(transaction);
        Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
        user_balance = userDatabaseHelper.userBalance(user_id);
        helper.updateCashAtHub(deviceid,offering_price.getText().toString());
        helper.storetranspublish(createUsbChargeString(),deviceid);
        helper.updateInventoryDecrease(offering_id);
        logoutConformation();
    }



    public String createUsbChargeString() {
        List<String> dataList = new ArrayList<>();

        String pub_id = String.valueOf(userDatabaseHelper.lastpub());
        timestamp = System.currentTimeMillis() / 1000L;
        String usersid = user_id;
        String transevent = transactionevent;
        String ubalance = userDatabaseHelper.userBalance(user_id);
        String amount = offering_price.getEditableText().toString();


        dataList.add(pub_id);
        dataList.add(enum_type);
        dataList.add(String.valueOf(timestamp));
        dataList.add(userDatabaseHelper.getoffering(offering_id));
        dataList.add(usersid);
        dataList.add(offering_id);
        dataList.add(usbchargepublish_id);
        dataList.add(amount);
        dataList.add(ubalance);
        dataList.add(attendent_id);

        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;
    }

    public void logoutConformation(){


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title
        alertDialogBuilder.setTitle(R.string.contin);

        // set dialog message
        alertDialogBuilder
                .setMessage(R.string.doyouwanttodoanothertrasacion)
                .setCancelable(false)
                .setPositiveButton(R.string.yes,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        Intent newIntent = new Intent(activity, CustomerMenu.class);
                        startActivity(newIntent);
                        dialog.cancel();
                        finish();
                    }
                })
                .setNegativeButton(R.string.no,new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                        logoutsharedpreference();
                        //bye bye
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public void logoutsharedpreference() {
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        finish();
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivity(newIntent);
    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, UsbCharge.class);
        finishAffinity();
        logoutsharedpreference();
        startActivity(newIntent);
        finish();
    }





}

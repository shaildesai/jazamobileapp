package com.jaza.shaildesai.jaza.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.jaza.shaildesai.jaza.Helpers.Helper;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.Model.User;
import com.jaza.shaildesai.jaza.R;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.ArrayList;
import java.util.List;

import static com.jaza.shaildesai.jaza.Activities.MainActivity.Attendent;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.IMEI;
import static com.jaza.shaildesai.jaza.Activities.MainActivity.Name;


public class UserUpdateActivity  extends AppCompatActivity {

    EditText first_name,last_name,phone_number,birth_year;
    String user_id;
    UserDatabaseHelper userDatabaseHelper;
    private String getdata;
    private SharedPreferences sharedpreferencesattendent;
    private String attendent_id;
    private Publish publish;
    private String csv;
    private String SEPERATOR =",";
    private String enum_type = "1";
    private String deviceid;
    private long timestamp;
    private StringBuilder csvList;
    private Helper helper;
    private String user_balance;
    private  String gender;


    protected  void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_update);
        getSupportActionBar().hide();
        SharedPreferences sharedpreferences = this.getApplication().getSharedPreferences(MainActivity.MyPrefences, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        userDatabaseHelper = new UserDatabaseHelper(this);
        helper = new Helper(this);
        user_id = sharedpreferences.getString(Name, "");
        deviceid = sharedpreferences.getString(IMEI,"");
        user_balance = userDatabaseHelper.userBalance(user_id);
        sharedpreferencesattendent= this.getApplication().getSharedPreferences(MainActivity.AttendentPref, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor1 = sharedpreferencesattendent.edit();
        attendent_id = sharedpreferencesattendent.getString(Attendent, "");



        User user = (User) getIntent().getExtras().getSerializable("USER");

        user_id = sharedpreferences.getString(Name,"");
        first_name= findViewById(R.id.firstname);
        last_name = findViewById(R.id.lastname);
        phone_number = findViewById(R.id.phonenumber);
        birth_year = findViewById(R.id.birthyear);



        first_name.setText(user.getFirst_name());
        last_name.setText(user.getLast_name());
        phone_number.setText(user.getPhone_number());
        birth_year.setText(user.getBirth_year());
        gender = user.getGender();

    }

    public void update(View view){

        String fname = first_name.getText().toString();
        String lname = last_name.getText().toString();
        String pnumber = phone_number.getText().toString();
        String  byear= birth_year.getText().toString();

        User user = new User(user_id,fname,lname,pnumber,byear);

       int result = userDatabaseHelper.updateUser(user);

       if(result > 0){
           helper.storedbasepublish(createUserUpdateString(),deviceid);
           Toast.makeText(this,"User Updated", Toast.LENGTH_SHORT).show();
       }

       else
       {
           Toast.makeText(this,"i am not updated", Toast.LENGTH_SHORT).show();
       }



    }

    public String createUserUpdateString() {
        List<String> dataList = new ArrayList<>();

        String publishid = String.valueOf(userDatabaseHelper.lastpub());
        String eventtype = enum_type;
        timestamp = System.currentTimeMillis() / 1000L;

        String usersid = user_id;
        String fname = first_name.getText().toString();
        String lname = last_name.getText().toString();
        String pnumber = phone_number.getText().toString();
        String byear = birth_year.getText().toString();
        String gtype = String.valueOf(gender);
        String attendentid = attendent_id;



        dataList.add(publishid);
        dataList.add(eventtype);
        dataList.add(String.valueOf(timestamp));
        dataList.add(usersid);
        dataList.add(fname);
        dataList.add(lname);
        dataList.add(pnumber);
        dataList.add(byear);
        dataList.add(gtype);
        dataList.add(attendentid);


        csvList = new StringBuilder();
        for (String s : dataList) {
            csvList.append(s);
            csvList.append(SEPERATOR);
            csv = csvList.toString();
            csv = csv.substring(0, csv.length() - SEPERATOR.length());
        }
        return csv;

    }

    @Override
    public void onBackPressed(){
        Intent newIntent = new Intent(this, CustomerMenu.class);
        finishAffinity();
        startActivity(newIntent);
        finish();
    }
}

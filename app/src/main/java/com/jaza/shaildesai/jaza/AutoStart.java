package com.jaza.shaildesai.jaza;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.jaza.shaildesai.jaza.Activities.GetService;

public class AutoStart extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, GetService.class));
    }
}


package com.jaza.shaildesai.jaza.Helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.jaza.shaildesai.jaza.Activities.MainActivity;
import com.jaza.shaildesai.jaza.Model.HubProperties;
import com.jaza.shaildesai.jaza.Model.Inventory;
import com.jaza.shaildesai.jaza.Model.Inventory_log;
import com.jaza.shaildesai.jaza.Model.PackStatus;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.Sql.UserDatabaseHelper;

import java.util.ArrayList;

public class Helper  {

    private Context context;
    public String Pack_at_hub = "6";
   public String pack_signed_out = "2";



    public Helper(Context context) {
        this.context = context;
    }

    public void storedbasepublish(String csv, String device_imei){

        UserDatabaseHelper userDatabaseHelper = new UserDatabaseHelper(context);
        Publish publish = new Publish();
        publish.setPub_id(userDatabaseHelper.lastpub());
        publish.setEvent("dbase_mob");
        publish.setData(String.valueOf(csv));
        publish.setDevice_id(device_imei);
        publish.setApp_version("1");
        userDatabaseHelper.addpublish(publish);

       // Toast.makeText(this, "Successfully published" , Toast.LENGTH_SHORT).show();
    }

    public void storetranspublish(String csv, String device_imei){

        UserDatabaseHelper userDatabaseHelper = new UserDatabaseHelper(context);
        Publish publish = new Publish();
        publish.setPub_id(userDatabaseHelper.lastpub());
        publish.setEvent("trans_mob");
        publish.setData(String.valueOf(csv));
        publish.setDevice_id(device_imei);
        publish.setApp_version("1");
        userDatabaseHelper.addpublish(publish);

        // Toast.makeText(this, "Successfully published" , Toast.LENGTH_SHORT).show();
    }

    public void storeInventoryLogs(int inventory_id, String log_type, int trans_id, int delta){

        UserDatabaseHelper userDatabaseHelper = new UserDatabaseHelper(context);
        Inventory_log inventory_log = new Inventory_log();
        inventory_log.setInventory_id(inventory_id);
        inventory_log.setLog_type(log_type);
        inventory_log.setTrans_id(String.valueOf(trans_id));
        inventory_log.setDelta(delta);

        userDatabaseHelper.addInventoryLogs(inventory_log);

        // Toast.makeText(this, "Successfully published" , Toast.LENGTH_SHORT).show();
    }

    public void updateInventoryDecrease(String offering_id) {
        ArrayList arrayList;
        UserDatabaseHelper userDatabaseHelper = new UserDatabaseHelper(context);
        String log_type = "1";
        arrayList = (userDatabaseHelper.findInventoryId(offering_id));
        int x = arrayList.size();

        for (int inv_id = 0; inv_id < x; inv_id++) {
            int in_d = Integer.valueOf(String.valueOf(arrayList.get(inv_id)));

            int amounti = userDatabaseHelper.findInventoryAmount(in_d);
            int trans_id = userDatabaseHelper.countmaxtransid();
            storeInventoryLogs(in_d, log_type, trans_id, (-amounti));
            int sum = userDatabaseHelper.sumOfQuantity(in_d);

            Inventory inventory = new Inventory(in_d,sum);
            int result = userDatabaseHelper.updatequantity(inventory);

            if(result > 0){
                // Toast.makeText(this,"User Updated", Toast.LENGTH_SHORT).show();
            }
            else
            {
                // Toast.makeText(this,"i am not updated", Toast.LENGTH_SHORT).show();
            }




        }


    }


    public void updateInventoryIncrease(String offering_id) {
        ArrayList arrayList;
        UserDatabaseHelper userDatabaseHelper = new UserDatabaseHelper(context);
        String log_type = "1";
        arrayList = (userDatabaseHelper.findInventoryId(offering_id));
        int x = arrayList.size();

        for (int inv_id = 0; inv_id < x; inv_id++) {
            int in_d = Integer.valueOf(String.valueOf(arrayList.get(inv_id)));

            int amounti = userDatabaseHelper.findInventoryAmount(in_d);
            int trans_id = userDatabaseHelper.countmaxtransid();
            storeInventoryLogs(in_d, log_type, trans_id, amounti);
            int sum = userDatabaseHelper.sumOfQuantity(in_d);

            Inventory inventory = new Inventory(in_d,sum);
            int result = userDatabaseHelper.updatequantity(inventory);

            if(result > 0){
                // Toast.makeText(this,"User Updated", Toast.LENGTH_SHORT).show();

            }
            else
            {
                // Toast.makeText(this,"i am not updated", Toast.LENGTH_SHORT).show();
            }




        }


    }


    public void update_packs(String pack, String user_id, String status,String time) {
        UserDatabaseHelper userDatabaseHelper = new UserDatabaseHelper(context);

        if (userDatabaseHelper.checkPackidInPacks(pack)) {
            String pack_status = status;
            String pack_user_id = user_id;
            String pack_time = time;
            String pack_id = pack;

            PackStatus packStatus = new PackStatus(pack_id, pack_status, pack_user_id, pack_time);

            userDatabaseHelper.updatePackStatus(packStatus);
        } else {
            storeTransactionoutpack(pack,user_id,status,time);
        }

    }

    private void storeTransactionoutpack(String pack, String user_id, String pack_status, String time) {
    PackStatus packStatus = new PackStatus();
    UserDatabaseHelper userDatabaseHelper = new UserDatabaseHelper(context);
        packStatus.setPacks_id(pack);
        packStatus.setLast_user_id(user_id);
        packStatus.setPack_state(pack_status);
        packStatus.setLast_transaction_date(time);

        userDatabaseHelper.addPackStatus(packStatus);
        // Toast.makeText(this, "Successfully inserted", Toast.LENGTH_SHORT).show();
    }

    public void updateCashAtHub(String deviceid, String payment_amount) {
        UserDatabaseHelper userDatabaseHelper = new UserDatabaseHelper(context);
        int payment = Integer.parseInt(payment_amount);
        int amount = (Integer.parseInt(userDatabaseHelper.lastCashAtHub(deviceid)) + payment);
        HubProperties hubProperties = new HubProperties(deviceid, amount);
        int finalamount = userDatabaseHelper.updateCashAtHub(hubProperties);
        if (finalamount > 0) {

        }


    }

}

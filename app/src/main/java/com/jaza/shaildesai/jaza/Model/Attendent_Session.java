package com.jaza.shaildesai.jaza.Model;

public class Attendent_Session {

    public int auto_att_id;
    public String att_id;
    public String time_in;
    public String time_out;

    public int getAuto_att_id() {
        return auto_att_id;
    }

    public void setAtt_id(String att_id) {
        this.att_id = att_id;
    }

    public String getAtt_id() {
        return att_id;
    }

    public void setAuto_att_id(int auto_att_id) {
        this.auto_att_id = auto_att_id;
    }

    public String getTime_in() {
        return time_in;
    }

    public void setTime_in(String time_in) {
        this.time_in = time_in;
    }

    public void setTime_out(String time_out) {
        this.time_out = time_out;
    }

    public String getTime_out() {
        return time_out;
    }

}

package com.jaza.shaildesai.jaza.Model;

public class HubProperties {

    private String hub_imei;
    private long cash_at_hub;
    private String last_remittance_amount;
    private String lastremittance_date;
    private String num_user;
    private String num_packs;
    private String num_publishes;
    private String num_publish_log;
    private String packs_out;
    private String packs_in;
    private String offering_version_id;
    private String max_remittance_id;
    private long total_cash_in;


    public HubProperties(String deviceid, long payment) {
        this.hub_imei = deviceid;
        this.cash_at_hub = payment;
    }

    public HubProperties() {

    }

    public HubProperties(String deviceid, long cash, String lastremittanceamount, String lastremittancedate, String numusers, String numpacks, String numpublish, String numpublishlogs, String numouts, String numin, String offeringversionid, String maxremittanceid, long total_cash_in) {
    this.hub_imei = deviceid;
    this.cash_at_hub = cash;
    this.last_remittance_amount = lastremittanceamount;
    this.lastremittance_date = lastremittancedate;
    this.num_user = numusers;
    this.num_packs = numpacks;
    this.num_publishes = numpublish;
    this.num_publish_log = numpublishlogs;
    this.packs_out = numouts;
    this.packs_in= numin;
    this.offering_version_id = offeringversionid;
    this.max_remittance_id = maxremittanceid;
    this.total_cash_in = total_cash_in;


    }

    public String getHub_imei() {
        return hub_imei;
    }

    public void setHub_imei(String hub_imei) {
        this.hub_imei = hub_imei;
    }

    public long getCash_at_hub() {
        return cash_at_hub;
    }

    public void setCash_at_hub(long cash_at_hub) {
        this.cash_at_hub = cash_at_hub;
    }

    public String getLast_remittance_amount() {
        return last_remittance_amount;
    }

    public void setLast_remittance_amount(String last_remittance_amount) {
        this.last_remittance_amount = last_remittance_amount;
    }

    public String getLastremittance_date() {
        return lastremittance_date;
    }

    public void setLastremittance_date(String lastremittance_date) {
        this.lastremittance_date = lastremittance_date;
    }

    public String getNum_user() {
        return num_user;
    }

    public void setNum_user(String num_user) {
        this.num_user = num_user;
    }

    public String getNum_packs() {
        return num_packs;
    }

    public void setNum_packs(String num_packs) {
        this.num_packs = num_packs;
    }

    public String getNum_publishes() {
        return num_publishes;
    }

    public void setNum_publishes(String num_publishes) {
        this.num_publishes = num_publishes;
    }

    public String getNum_publish_log() {
        return num_publish_log;
    }

    public void setNum_publish_log(String num_publish_log) {
        this.num_publish_log = num_publish_log;
    }

    public String getPacks_in() {
        return packs_in;
    }

    public void setPacks_in(String packs_in) {
        this.packs_in = packs_in;
    }

    public String getPacks_out() {
        return packs_out;
    }

    public void setPacks_out(String packs_out) {
        this.packs_out = packs_out;
    }

    public String getOffering_version_id() {
        return offering_version_id;
    }

    public void setOffering_version_id(String offering_version_id) {
        this.offering_version_id = offering_version_id;
    }

    public String getMax_remittance_id() {
        return max_remittance_id;
    }

    public void setMax_remittance_id(String max_remittance_id) {
        this.max_remittance_id = max_remittance_id;
    }

    public long getTotal_cash_in() {
        return total_cash_in;
    }

    public void setTotal_cash_in(long total_cash_in) {
        this.total_cash_in = total_cash_in;
    }
}

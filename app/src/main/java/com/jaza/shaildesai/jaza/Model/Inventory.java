package com.jaza.shaildesai.jaza.Model;

import java.io.Serializable;

public class Inventory implements Serializable {

    public int inventory_id;
    public String inventory_name;
    public int inventory_amount;

    public Inventory(int inventor_id,int newamount) {
        this.inventory_id = inventor_id;
        this.inventory_amount = newamount;
    }

    public Inventory() {

    }

    public int getInventory_id() {
        return inventory_id;
    }

    public void setInventory_id(int inventory_id) {
        this.inventory_id = inventory_id;
    }

    public String getInventory_name() {
        return inventory_name;
    }

    public void setInventory_name(String inventory_name) {
        this.inventory_name = inventory_name;
    }

    public int getInventory_amount() {
        return inventory_amount;
    }

    public void setInventory_amount(int inventory_amount) {
        this.inventory_amount = inventory_amount;
    }
}

package com.jaza.shaildesai.jaza.Model;

public class Inventory_log {

    public int log_id;
    public int inventory_id;
    public String log_type;
    public String trans_id;
    public int cloud_log_id;
    public int delta;

    public int getLog_id() {
        return log_id;
    }

    public void setLog_id(int log_id) {
        this.log_id = log_id;
    }

    public int getInventory_id() {
        return inventory_id;
    }

    public void setInventory_id(int inventory_id) {
        this.inventory_id = inventory_id;
    }

    public String getLog_type() {
        return log_type;
    }

    public void setLog_type(String log_type) {
        this.log_type = log_type;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public int getCloud_log_id() {
        return cloud_log_id;
    }

    public void setCloud_log_id(int cloud_log_id) {
        this.cloud_log_id = cloud_log_id;
    }

    public int getDelta() {
        return delta;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }
}

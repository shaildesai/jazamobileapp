package com.jaza.shaildesai.jaza.Model;

public class Offering_item  {
    public String offering_item_offering_id;
    public int offering_item_Inventory_id;
    public int Offering_item_amount;



    public Offering_item() {

    }

    public Offering_item(int inventory_id) {
        this.offering_item_Inventory_id = inventory_id;

    }


    public String getOffering_item_offering_id() {
        return offering_item_offering_id;
    }

    public void setOffering_item_offering_id(String offering_item_offering_id) {
        this.offering_item_offering_id = offering_item_offering_id;
    }

    public int getOffering_item_Inventory_id() {
        return offering_item_Inventory_id;
    }

    public void setOffering_item_Inventory_id(int offering_item_Inventory_id) {
        this.offering_item_Inventory_id = offering_item_Inventory_id;
    }

    public int getOffering_item_amount() {
        return Offering_item_amount;
    }

    public void setOffering_item_amount(int offering_item_amount) {
        Offering_item_amount = offering_item_amount;
    }

    @Override
    public String toString() {
        return String.valueOf(offering_item_Inventory_id);
    }

}


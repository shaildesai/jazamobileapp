package com.jaza.shaildesai.jaza.Model;

import java.io.Serializable;

public class Offerings implements Serializable {

    public String offering_id;
    public String offering_type;
    public int offering_version_id;
    public String offering_name;
    public String price;
    public String min_down_payment;
    public String duration;
    public String created_at;

    public Offerings(String offering_id,String offering_type, String offering_name, String price, String min_down_payment,String duration) {
        this.offering_id = offering_id;
        this.offering_type = offering_type;
        this.offering_name = offering_name;
        this.price = price;
        this.min_down_payment = min_down_payment;
        this.duration = duration;



    }

    public Offerings() {

    }

    public Offerings(String offering_id, int offeringversion) {
        this.offering_id = offering_id;
        this.offering_version_id = offeringversion;
    }


    public String getOffering_id() {
        return offering_id;
    }

    public void setOffering_id(String offering_id) {
        this.offering_id = offering_id;
    }


    public String getOffering_type() {
        return offering_type;
    }

    public void setOffering_type(String offering_type) {
        this.offering_type = offering_type;
    }

    public int getOffering_version_id() {
        return offering_version_id;
    }

    public void setOffering_version_id(int offering_version_id) {
        this.offering_version_id = offering_version_id;
    }

    public String getOffering_name() {
        return offering_name;
    }

    public void setOffering_name(String offering_name) {
        this.offering_name = offering_name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMin_down_payment() {
        return min_down_payment;
    }

    public void setMin_down_payment(String min_down_payment) {
        this.min_down_payment = min_down_payment;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration() {
        return duration;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }


    @Override
    public String toString() {
        return offering_name;
    }

}

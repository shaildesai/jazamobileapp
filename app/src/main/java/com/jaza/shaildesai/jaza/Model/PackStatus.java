package com.jaza.shaildesai.jaza.Model;

public class PackStatus {

    private String packs_id;
    private String pack_state;
    private String last_user_id;
    private String last_transaction_date;

    public PackStatus(String pack_id, String pack_status, String pack_user_id, String pack_time) {
        this.packs_id = pack_id;
        this.pack_state = pack_status;
        this.last_user_id = pack_user_id;
        this.last_transaction_date = pack_time;
    }

    public PackStatus() {

    }

    public String getPacks_id() {
        return packs_id;
    }

    public void setPacks_id(String packs_id) {
        this.packs_id = packs_id;
    }

    public String getPack_state() {
        return pack_state;
    }

    public void setPack_state(String pack_state) {
        this.pack_state = pack_state;
    }

    public String getLast_user_id() {
        return last_user_id;
    }

    public void setLast_user_id(String last_user_id) {
        this.last_user_id = last_user_id;
    }

    public String getLast_transaction_date() {
        return last_transaction_date;
    }

    public void setLast_transaction_date(String last_transaction_date) {
        this.last_transaction_date = last_transaction_date;
    }
}

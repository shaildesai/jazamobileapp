package com.jaza.shaildesai.jaza.Model;

import java.io.Serializable;

public class Publish implements Serializable {

    private Integer pub_id;
    private String event;
    private String data;
    private String device_id;
    private String app_version;
    private int successful;
    private String Response;

    public Publish(int pid, String pevent, String pdata, String pdevice, String pappversion) {
        this.pub_id = pid;
        this.event = pevent;
        this.data = pdata;
        this.data = pdevice;
        this.app_version= pappversion;
    }

    public Publish() {

    }

    public Publish(int pub_id, int sucessfult) {this.pub_id= pub_id;
    this.successful = sucessfult;
    }


    public Integer getPub_id() {
        return pub_id;
    }

    public void setPub_id(Integer pub_id) {
        this.pub_id = pub_id;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public int getSuccessful() {
        return successful;
    }

    public void setSuccessful(int successful) {
        this.successful = successful;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String response) {
        Response = response;
    }
}

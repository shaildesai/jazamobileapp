package com.jaza.shaildesai.jaza.Model;

public class Remittance {


    private  int  remiitance_id;
    private String remittance_type;
    private  String amount;
    private String date;
    private String sent_to;

    public int getRemiitance_id() {
        return remiitance_id;
    }

    public void setRemiitance_id(int remiitance_id) {
        this.remiitance_id = remiitance_id;
    }

    public String getRemittance_type() {
        return remittance_type;
    }

    public void setRemittance_type(String remittance_type) {
        this.remittance_type = remittance_type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSent_to() {
        return sent_to;
    }

    public void setSent_to(String sent_to) {
        this.sent_to = sent_to;
    }
}

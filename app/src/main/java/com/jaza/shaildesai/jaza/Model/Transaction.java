package com.jaza.shaildesai.jaza.Model;

public class Transaction {
    public int trans_id;
    public String User_Id;
    public String trans_type;
    public String pack_in_id;
    public String pack_out_id;
    public String offering_id;
    public String payment_amount;
    public String attendent_id;
    public String  transaction_type_enum;
    public String Created_at;
    public String Updated_at;

    public int getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(int trans_id) {
        this.trans_id = trans_id;
    }

    public String getUser_Id() {
        return User_Id;
    }

    public void setUser_Id(String user_Id) {
        User_Id = user_Id;
    }

    public String getTrans_type() {
        return trans_type;
    }

    public void setTrans_type(String trans_type) {
        this.trans_type = trans_type;
    }

    public String getPack_in_id() {
        return pack_in_id;
    }

    public void setPack_in_id(String pack_in_id) {
        this.pack_in_id = pack_in_id;
    }

    public String getPack_out_id() {
        return pack_out_id;
    }

    public void setPack_out_id(String pack_out_id) {
        this.pack_out_id = pack_out_id;
    }

    public String getOffering_id() {
        return offering_id;
    }

    public void setOffering_id(String offering_id) {
        this.offering_id = offering_id;
    }

    public String getPayment_amount() {
        return payment_amount;
    }

    public void setPayment_amount(String payment_amount) {
        this.payment_amount = payment_amount;
    }

    public String getAttendent_id() {
        return attendent_id;
    }

    public void setAttendent_id(String attendent_id) {
        this.attendent_id = attendent_id;
    }

    public String getTransaction_type_enum() {
        return transaction_type_enum;
    }

    public void setTransaction_type_enum(String transaction_type_enum) {
        this.transaction_type_enum = transaction_type_enum;
    }

    public String getCreated_at() {
        return Created_at;
    }

    public void setCreated_at(String created_at) {
        Created_at = created_at;
    }

    public String getUpdated_at() {
        return Updated_at;
    }

    public void setUpdated_at(String updated_at) {
        Updated_at = updated_at;
    }


}

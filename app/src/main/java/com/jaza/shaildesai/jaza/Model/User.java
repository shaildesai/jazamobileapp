package com.jaza.shaildesai.jaza.Model;

import java.io.Serializable;

public class User implements Serializable {

    public String user_id;
    public String first_name;
    public String last_name;
    public String phone_number;
    public String birth_year;
    public String gender;
    public String event_type;
    public String attendent;
    public String loan_amount;
    public String downpayment;
    public String balance;
    public String loan_last_payment;
    public String user_membership_expiray_date;
    public String user_created_at;
    public String user_updated_at;

    public User() {
    }

    public User(String user_id, String first_name, String last_name, String phone_number, String birth_year) {
        this.user_id = user_id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone_number = phone_number;
        this.birth_year= birth_year;
    }



    public User(String user_id, String loan_amount, String downpayment, String balance) {
        this.user_id = user_id;
        this.loan_amount = loan_amount;
        this.downpayment = downpayment;
        this.balance = balance;
    }

    public User(String balance) {
        this.balance= balance;
    }

    public User(String user_id, String balance, String lastpayment) {
        this.user_id = user_id;
        this.balance = balance;
        this.loan_last_payment = lastpayment;
    }

    public User(String user_id, String membershipdate) {
        this.user_id = user_id;
        this.user_membership_expiray_date = membershipdate;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFirst_name(){
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone_number(){
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getBirth_year() {
        return birth_year;
    }

    public void setBirth_year(String birth_year) {
        this.birth_year = birth_year;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getAttendent(){
        return attendent;
    }

    public void setAttendent(String attendent) {
        this.attendent = attendent;
    }

    public String getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(String loan_amount) {
        this.loan_amount = loan_amount;
    }

    public String getDownpayment() {
        return downpayment;
    }

    public void setDownpayment(String downpayment) {
        this.downpayment = downpayment;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getLoan_last_payment() {
        return loan_last_payment;
    }

    public void setLoan_last_payment(String loan_last_payment) {
        this.loan_last_payment = loan_last_payment;
    }

    public String getUser_created_at() {
        return user_created_at;
    }

    public void setUser_created_at(String user_created_at) {
        this.user_created_at = user_created_at;
    }

    public  String getUser_membership_expiray_date(){
        return user_membership_expiray_date;
    }

    public void setUser_membership_expiray_date(String user_membership_expiray_date) {
        this.user_membership_expiray_date = user_membership_expiray_date;
    }

    public String getUser_updated_at() {
        return user_updated_at;
    }

    public void setUser_updated_at(String user_updated_at) {
        this.user_updated_at = user_updated_at;
    }



    @Override
    public String toString() {
        return first_name;
    }



}
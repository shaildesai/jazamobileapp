package com.jaza.shaildesai.jaza.Sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import com.jaza.shaildesai.jaza.Model.Attendent_Session;
import com.jaza.shaildesai.jaza.Model.HubProperties;
import com.jaza.shaildesai.jaza.Model.Inventory;
import com.jaza.shaildesai.jaza.Model.Inventory_log;
import com.jaza.shaildesai.jaza.Model.Offering_item;
import com.jaza.shaildesai.jaza.Model.Offerings;
import com.jaza.shaildesai.jaza.Model.PackStatus;
import com.jaza.shaildesai.jaza.Model.Publish;
import com.jaza.shaildesai.jaza.Model.PublishLog;
import com.jaza.shaildesai.jaza.Model.Remittance;
import com.jaza.shaildesai.jaza.Model.Transaction;
import com.jaza.shaildesai.jaza.Model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.app.PendingIntent.getActivity;

public class UserDatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Jaza.db";
    private static final String TABLE_USER = "users";
    private static final String USER_ID = "user_id";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String PHONE_NUMBER = "phone_number";
    private static final String BIRTH_YEAR = "birth_year";
    private static final String GENDER = "gender";
    private static final String EVENT_TYPE = "event_type";
    private static final String ATTENDANT = "attendant";
    private static final String LOAN_AMOUNT = "loan_amount";
    private static final String DOWNPAYMENT = "down_payment";
    private static final String BALANCE = "balance";
    private static final String LOAN_LAST_PAYMENT = "loan_last_payment";
    private static final String MEMBERSHIP_EXPIRARY_DATE = "membership_expirary_date";
    private static final String USER_CREATED_AT = "user_created_at";
    private static final String USER_UPDATED_AT = "user_updated_at";
    public String first_name;
    public String last_name;


    // offering tables
    private static final String TABLE_OFFERINGS = "Offerings";
    private static final String OFFERING_ID = "offerings_id";
    private static final String OFFERING_TYPE = "offerings_type";
    private static final String OFFERING_VERSION_ID = "offering_version_id";
    private static final String OFFERING_NAME = "offering_name";
    private static final String PRICE = "price";
    private static final String MIN_DOWN_PAYEMNT = "min_down_payment";
    private static final String DURATION = "duration";
    private static final String CREATED_AT = "created_at";
    //private static final String filterloa = "loan";
    Cursor cursor1;


    //Transaction table variables

    private static final String TABLE_TRANSACTION = "Transactions";
    private static final String TRANS_ID = "trans_id";
    private static final String USER_ID_TRANS = "user_id";
    private static final String TRANS_TYPE = "trans_type";
    private static final String PACK_IN_ID = "pack_in_id";
    private static final String PACK_OUT_ID = "pack_out_id";
    private static final String OFFERING_ID_TRANS = "offering_id";
    private static final String PAYMENT_AMOUNT = "payment_amount";
    private static final String ATTENDENT_ID = "attendent_id";
    private static final String TRANSACTION_TYPE_ENUM = "Transaction_type_enum";
    private static final String CREATED_AT_TRANS = "created_at";
    private static final String UPDATED_AT = "updated_at";


    //Publish_table variables
    private static final String TABLE_PUBLISH = "Publishing_table";
    private static final String PUB_ID = "pub_id";
    private static final String EVENT = "event";
    private static final String DATA = "data";
    private static final String DEVICE_ID = "Device_id";
    private static final String APP_VERSION = "app_version";
    private static final String SUCCESSFUL = "successful";
    private static final String RESPONSE = "response";
    Boolean rowExists;

    //rOffering_items version Table varibale declaration
    private static final String TABLE_OFFERING_ITEMS = "Offering_items";
    private static final String OFFERING_ITEM_ID = "offering_item_id";
    private static final String OFFERING_ITEM_OFF_ID = "offering_id";
    private static final String INVENTORY_ID = "inventory_id";
    private static final String AMOUNT = "amount";

    //Inventory Table variable declaration;
    private static final String TABLE_INVENTORY = "Inventory";
    private static final String INVENTORY_INVENTORY_ID = "inventory_Id";
    private static final String INVENTORY_NAME = "name";
    private static final String INVENTORY_AMOUNT = "amount";

    //Remittance Table variable declaration
    private static final String TABLE_REMITTANCE = "Remittance";
    private static final String REMITTANCE_U_ID = "id";
    private static final String REMITTANCE_ID = "remittance_id";
    private static final String REMITTANCE_TYPE = "remittance_type";
    private static final String REMITTANCE_AMOUNT = "amount";
    private static final String REMITTANCE_DATE = "date";
    private static final String REMITTANCE_SENT_TO = "remittance_sent_to";


    //Hub Properties Table variable declaration
    private static final String TABLE_HUB_PROPERTIES = "Hub_properties";
    private static final String HUB_ID_IMEI = "hub_id_imei";
    private static final String CASH_AT_HUB = "cash_at_hub";
    private static final String LAST_REMIITANCE_AMOUNT = "last_remittance_amount";
    private static final String LAST_REMIITANCE_DATE = "last_remittance_date";
    private static final String NUM_USERS = "num_users";
    private static final String NUM_PACKS = "num_packs";
    private static final String NUM_PUBLISHES = "nun_publishes";
    private static final String NUM_PUBLISH_LOG = "num_publish_logs";
    private static final String PACKS_OUT = "packs_out";
    private static final String PACK_IN = "packs_in";
    private static final String HUB_PROPERTIES_OFFERING_VERSION_ID = "offering_version_id";
    private static final String MAX_REMITTANCE_ID = "max_remittance_id";
    private static final String TOTAL_CASH_IN = "total_cash_in";


    //Packs Table variable declaration
    private static final String TABLE_PACKS = "Packs";
    private static final String PACK_ID = "pack_id";
    private static final String PACK_STATE = "pack_state";
    private static final String LAST_USER_ID = "last_user_id";
    private static final String LASTTRANSACTION_DATE = "last_transaction_date";

    //inventory_log_variable
    private static final String TABLE_INVENTORY_LOG = "Inventory_Log";
    private static final String LOG_ID = "log_id";
    private static final String ILOGS_INVENTORY_ID = "inventory_id";
    private static final String ILOG_TYPE = "log_type";
    private static final String ILOG_TRANS_ID = "trans_id";
    private static final String CLOUD_LOG_ID = "cloud_log_id";
    private static final String DELTA = "delta";

    //Attendent table_log_variable
    private static final String TABLE_ATTENDANT_SESSION = "Attendent_session";
    private static final String ATT_ID = "id";
    private static final String ATTENDENTABLE_ATTENDENT_ID = "attendent_id";
    private static final String TIME_IN = "time_in";
    private static final String TIME_OUT = "time_out";

    //Publish_backlog
    private static final String TABLE_PUBLISH_BACKLOG = "Publish_back_log";
    private static final String PUBLISH_BACKLOG_ID = "publish_backlog_id";


    private String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "(" + USER_ID + " TEXT PRIMARY KEY," + FIRST_NAME + " TEXT," + LAST_NAME + " TEXT," + PHONE_NUMBER + " TEXT," + BIRTH_YEAR + " TEXT," + GENDER + " TEXT," + EVENT_TYPE + " TEXT," + ATTENDANT + " TEXT," + DOWNPAYMENT + " TEXT," + BALANCE + " TEXT DEFAULT 0 ," + LOAN_AMOUNT + " TEXT DEFAULT 0," + LOAN_LAST_PAYMENT + " TEXT DEFAULT 0," + MEMBERSHIP_EXPIRARY_DATE + " TEXT," + USER_CREATED_AT + " TIMESTAMP NOT NULL DEFAULT current_timestamp, " + USER_UPDATED_AT + " TIMESTAMP NOT NULL DEFAULT current_timestamp" + ")";

    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_USER;


    //offering table creation
    private String CREATE_OFFERING_TABLE = "CREATE TABLE " + TABLE_OFFERINGS + "(" + OFFERING_ID + " TEXT, " + OFFERING_TYPE + " TEXT, " + OFFERING_VERSION_ID + " INTEGER DEFAULT 0," + OFFERING_NAME + " TEXT," + PRICE + " TEXT," + MIN_DOWN_PAYEMNT + " TEXT," + DURATION + " TEXT," + CREATED_AT + " TIMESTAMP NOT NULL DEFAULT current_timestamp" + ")";

    private String DROP_OFFERING_TABLE = "DROP TABLE IF EXISTS " + TABLE_OFFERINGS;

    //Create offering_items_id
    private String CREATE_OFFERING_ITEMS_TABLE = "CREATE TABLE " + TABLE_OFFERING_ITEMS + "(" + OFFERING_ITEM_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + OFFERING_ITEM_OFF_ID + " TEXT," + INVENTORY_ID + " INTEGER," + AMOUNT + " INTEGER" + ")";

    private String DROP_OFFERING_ITEM_TABLE = "DROP TABLE IF EXISTS " + TABLE_OFFERING_ITEMS;

    //Create Inventroty Table
    private String CREATE_INVENTORY_TABLE = "CREATE TABLE " + TABLE_INVENTORY + "(" + INVENTORY_INVENTORY_ID + " INTEGER , " + INVENTORY_NAME + " TEXT," + INVENTORY_AMOUNT + " INTEGER" + ")";

    private String DROP_INVENTORY_TABLE = "DROP TABLE IF EXISTS " + TABLE_INVENTORY;


    //Creating transaction table
    private String CREATE_TRANSACTION_TABLE = "CREATE TABLE " + TABLE_TRANSACTION + "(" + TRANS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + USER_ID_TRANS + " TEXT," + TRANS_TYPE + " TEXT," + PACK_IN_ID + " TEXT," + PACK_OUT_ID + " TEXT," + OFFERING_ID_TRANS + " TEXT, " + PAYMENT_AMOUNT + " TEXT," + ATTENDENT_ID + " Text," + TRANSACTION_TYPE_ENUM + " TEXT, " + CREATED_AT_TRANS + " TIMESTAMP NOT NULL DEFAULT current_timestamp, " + UPDATED_AT + " TIMESTAMP NOT NULL DEFAULT current_timestamp " + ")";
    private String DROP_TRANSACTION_TABLE = "DROP TABLE IF EXISTS " + TABLE_TRANSACTION;


    //Create Publish Table
    private String CREATE_PUBLISH_TABLE = "CREATE TABLE " + TABLE_PUBLISH + "(" + PUB_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + EVENT + " TEXT, " + DATA + " TEXT, " + DEVICE_ID + " TEXT, " + APP_VERSION + " TEXT, " + SUCCESSFUL + " INTEGER DEFAULT 0, " + RESPONSE + " TEXT " + ")";

    private String DROP_PUBLISH_TABLE = "DROP TABLE IF EXISTS " + TABLE_PUBLISH;

    //Create Remittance Table
    private String CREATE_REMITTANCE_TABLE = "CREATE TABLE " + TABLE_REMITTANCE + "(" + REMITTANCE_U_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + REMITTANCE_ID + " INTEGER," + REMITTANCE_TYPE + " TEXT, " + REMITTANCE_AMOUNT + " TEXT," + REMITTANCE_DATE + " TEXT," + REMITTANCE_SENT_TO + " TEXT" + ")";

    private String DROP_REMITTANCE_TABLE = "DROP TABLE IF EXISTS " + TABLE_REMITTANCE;

    //Create Hub_properties_table

    private String CREATE_HUB_PROPERTIES = "CREATE TABLE " + TABLE_HUB_PROPERTIES + "(" + HUB_ID_IMEI + " TEXT," + CASH_AT_HUB + " TEXT," + LAST_REMIITANCE_AMOUNT + " TEXT, " + LAST_REMIITANCE_DATE + " TEXT," + NUM_USERS + " TEXT," + NUM_PACKS + " TEXT," + NUM_PUBLISHES + " TEXT," + NUM_PUBLISH_LOG + " TEXT," + PACKS_OUT + " TEXT," + PACK_IN + " TEXT," + HUB_PROPERTIES_OFFERING_VERSION_ID + " TEXT," + MAX_REMITTANCE_ID + " TEXT," + TOTAL_CASH_IN + " TEXT " + ")";

    private String DROP_HUB_PROPERTIES = "DROP TABLE IF EXISTS " + TABLE_HUB_PROPERTIES;

    //Create Packs Table
    private String CREATE_PACKS = "CREATE TABLE " + TABLE_PACKS + "(" + PACK_ID + " TEXT," + PACK_STATE + " TEXT," + LAST_USER_ID + " TEXT," + LASTTRANSACTION_DATE + " TEXT" + ")";

    private String DROP_PACKS = "DROP TABLE IF EXISTS " + TABLE_PACKS;

    //Create Inventory_log Table

    private String CREATE_INVENTORY_LOG = "CREATE TABLE " + TABLE_INVENTORY_LOG + "(" + LOG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + ILOGS_INVENTORY_ID + " INTEGER," + ILOG_TYPE + " TEXT," + ILOG_TRANS_ID + " TEXT," + CLOUD_LOG_ID + " INTEGER," + DELTA + " INTEGER" + ")";

    private String DROP_INVENTORY_LOG = "DROP TABLE IF EXISTS " + TABLE_INVENTORY_LOG;

    //Attendent_session table

    private String CREATE_ATTENDENT_SESSION_TABLE = "CREATE TABLE " + TABLE_ATTENDANT_SESSION + "(" + ATT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + ATTENDENTABLE_ATTENDENT_ID + " TEXT," + TIME_IN + " TEXT," + TIME_OUT + " TEXT" + ")";

    private String DROP_CREATE_ATTENDENT_SEESION_TABLE = "DROP TABLE IF EXISTS " + TABLE_ATTENDANT_SESSION;

    //Publish_BackLog

    private String CREATE_PUBLISHBACKLOG_TABLE = "CREATE TABLE " + TABLE_PUBLISH_BACKLOG + "(" + PUBLISH_BACKLOG_ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + ")";
    private String DROP_PUBLISBACKLOG_TABLE = "DROP TABLE IF EXISTS " + TABLE_PUBLISH_BACKLOG;


    public UserDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(CREATE_USER_TABLE);
        db.execSQL(CREATE_OFFERING_TABLE);
        db.execSQL(CREATE_TRANSACTION_TABLE);
        db.execSQL(CREATE_PUBLISH_TABLE);
        db.execSQL(CREATE_OFFERING_ITEMS_TABLE);
        db.execSQL(CREATE_INVENTORY_TABLE);
        db.execSQL(CREATE_REMITTANCE_TABLE);
        db.execSQL(CREATE_HUB_PROPERTIES);
        db.execSQL(CREATE_PACKS);
        db.execSQL(CREATE_INVENTORY_LOG);
        db.execSQL(CREATE_ATTENDENT_SESSION_TABLE);
        db.execSQL(CREATE_PUBLISHBACKLOG_TABLE);


    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL(DROP_USER_TABLE);
        db.execSQL(DROP_OFFERING_TABLE);
        db.execSQL(DROP_TRANSACTION_TABLE);
        db.execSQL(DROP_PUBLISH_TABLE);
        db.execSQL(DROP_OFFERING_ITEM_TABLE);
        db.execSQL(DROP_INVENTORY_TABLE);
        db.execSQL(DROP_REMITTANCE_TABLE);
        db.execSQL(DROP_HUB_PROPERTIES);
        db.execSQL(DROP_PACKS);
        db.execSQL(DROP_INVENTORY_TABLE);
        db.execSQL(DROP_INVENTORY_LOG);
        db.execSQL(DROP_CREATE_ATTENDENT_SEESION_TABLE);
        db.execSQL(DROP_PUBLISBACKLOG_TABLE);
        onCreate(db);

    }


    public void addUser(User user) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(String.valueOf(USER_ID), user.getUser_id());
        values.put(FIRST_NAME, user.getFirst_name());
        values.put(LAST_NAME, user.getLast_name());
        values.put(String.valueOf(PHONE_NUMBER), user.getPhone_number());
        values.put(BIRTH_YEAR, user.getBirth_year());
        values.put(GENDER, user.getGender());
        values.put(String.valueOf(ATTENDANT), user.getAttendent());

        db.insert(TABLE_USER, null, values);
        db.close();

    }


    public int updateUser(User user) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(FIRST_NAME, user.getFirst_name());
        values.put(LAST_NAME, user.getLast_name());
        values.put(PHONE_NUMBER, user.getPhone_number());
        values.put(BIRTH_YEAR, user.getBirth_year());


        return db.update(TABLE_USER, values, USER_ID + " = ? ", new String[]{user.getUser_id()});


    }

    public int updateUserBalance(User user) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(BALANCE, user.getBalance());
        values.put(LOAN_LAST_PAYMENT, user.getLoan_last_payment());


        return db.update(TABLE_USER, values, USER_ID + " = ? ", new String[]{user.getUser_id()});


    }


    public int updateUserMembershipDate(User user) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MEMBERSHIP_EXPIRARY_DATE, user.getUser_membership_expiray_date());


        return db.update(TABLE_USER, values, USER_ID + " = ? ", new String[]{user.getUser_id()});


    }


    public int updateUserLoan(User user) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(LOAN_AMOUNT, user.getLoan_amount());
        values.put(DOWNPAYMENT, user.getDownpayment());
        values.put(BALANCE, user.getBalance());
        int x = db.update(TABLE_USER, values, USER_ID + " = ? ", new String[]{user.getUser_id()});
        db.close();
        return x;


    }


    public boolean checkUser(String user_id) {
        String[] columns = {USER_ID};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = USER_ID + " = ?";
        String[] selectionArgs = {user_id};

        Cursor cursor = db.query(TABLE_USER, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;

    }

    public boolean checkbalance(String user_id) {

        String[] columns = {BALANCE};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = BALANCE + " IS NOT NULL OR " + BALANCE + " = 0 " + USER_ID + " = " + user_id;


        Cursor cursor = db.rawQuery("select " + BALANCE + " from " + TABLE_USER + " where " + USER_ID + " = ? AND " + BALANCE + " > 0 ", new String[]{user_id});
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;

    }


    public List<User> getsearcheduser(String searchstring) {
        List<User> userList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + USER_ID + ", " + FIRST_NAME + ", " + LAST_NAME + ", " + PHONE_NUMBER + ", " + BIRTH_YEAR + " from " + TABLE_USER + " Where " + FIRST_NAME + "  like '%" + searchstring + "%'", null);
        if (cursor.moveToFirst()) {
            do {
                String userid = cursor.getString(0);
                String fname = cursor.getString(1);
                String lname = cursor.getString(2);
                String pnumber = cursor.getString(3);
                String byear = cursor.getString(4);

                User user = new User(userid, fname, lname, pnumber, byear);
                userList.add(user);


            } while (cursor.moveToNext());
        }
        cursor.close();
        return userList;
    }


    public List<User> getallusers() {
        List<User> userList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select " + USER_ID + ", " + FIRST_NAME + ", " + LAST_NAME + ", " + PHONE_NUMBER + ", " + BIRTH_YEAR + " from " + TABLE_USER, null);
        if (cursor.moveToFirst()) {
            do {
                String userid = cursor.getString(0);
                String fname = cursor.getString(1);
                String lname = cursor.getString(2);
                String pnumber = cursor.getString(3);
                String byear = cursor.getString(4);

                User user = new User(userid, fname, lname, pnumber, byear);
                userList.add(user);


            } while (cursor.moveToNext());
        }
        cursor.close();
        return userList;
    }


    public List<User> getdata(String user_id) {
        List<User> userList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String selection = USER_ID + " = ?";
        String[] selectionArgs = {user_id};
        Cursor cursor = db.rawQuery("select " + USER_ID + ", " + FIRST_NAME + ", " + LAST_NAME + ", " + PHONE_NUMBER + ", " + BIRTH_YEAR + " from " + TABLE_USER + " Where " + selection, selectionArgs);
        if (cursor.moveToFirst()) {
            do {
                String userid = cursor.getString(0);
                String fname = cursor.getString(1);
                String lname = cursor.getString(2);
                String pnumber = cursor.getString(3);
                String byear = cursor.getString(4);

                User user = new User(userid, fname, lname, pnumber, byear);
                userList.add(user);


            } while (cursor.moveToNext());
        }
        cursor.close();
        return userList;
    }

    public String userBalance(String user_id) {
        String blance = "0";
        String[] columns = {BALANCE};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = USER_ID + " = ?";
        String[] selectionArgs = {user_id};

        Cursor cursor = db.query(TABLE_USER, columns, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) { // Here we try to move to the first record
            blance = String.valueOf(cursor.getString(0));
            if (blance == null) {
                blance = "0";

            }
        }

        // Onlry assign string value if we moved to first record
        return blance;

    }

    public String userMembershipDate(String user_id) {
        String date = "0";
        String[] columns = {MEMBERSHIP_EXPIRARY_DATE};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = USER_ID + " = ?";
        String[] selectionArgs = {user_id};

        Cursor cursor = db.query(TABLE_USER, columns, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) { // Here we try to move to the first record
            date = String.valueOf(cursor.getString(0));
            if (date == null) {
                date = "0";

            }
        }

        // Onlry assign string value if we moved to first record
        return date;

    }


    public String userDownpayment(String user_id) {
        String blance = "0";
        String[] columns = {DOWNPAYMENT};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = USER_ID + " = ?";
        String[] selectionArgs = {user_id};

        Cursor cursor = db.query(TABLE_USER, columns, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) // Here we try to move to the first record
            blance = String.valueOf(cursor.getString(0));
        cursor.close();
        return blance;
        // Only assign string value if we moved to first record


    }

    public String userLoanAmount(String user_id) {
        String blance = "0";
        String[] columns = {LOAN_AMOUNT};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = USER_ID + " = ?";
        String[] selectionArgs = {user_id};

        Cursor cursor = db.query(TABLE_USER, columns, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) // Here we try to move to the first record
            blance = String.valueOf(cursor.getString(0));
        cursor.close();
        return blance;
        // Only assign string value if we moved to first record


    }


    //Offerings methods

    public boolean checkofferings(String offeringname) {

        String[] columns = {OFFERING_NAME};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = OFFERING_NAME + " = ?";
        String[] selectionArgs = {offeringname};

        Cursor cursor = db.query(TABLE_OFFERINGS, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;

    }

    public boolean checkName() {
        String[] columns = {OFFERING_NAME};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = OFFERING_NAME + " LIKE ?";
        String[] selectionArgs = {"'%' + loan + '%'"};

        Cursor cursor = db.query(TABLE_OFFERINGS, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;

    }


    public void addofferings(Offerings offerings) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(OFFERING_ID, offerings.getOffering_id());
        values.put(OFFERING_TYPE, offerings.getOffering_type());
        values.put(OFFERING_NAME, offerings.getOffering_name());
        values.put(PRICE, offerings.price);
        values.put(MIN_DOWN_PAYEMNT, offerings.min_down_payment);
        values.put(DURATION, offerings.getDuration());
        values.put(OFFERING_VERSION_ID,offerings.getOffering_version_id());


        // Inserting Row
        db.insert(TABLE_OFFERINGS, null, values);
        db.close();
    }

    public boolean checkoffering_id(String offering_id) {

        String[] columns = {OFFERING_ID};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = OFFERING_ID + " = ?";
        String[] selectionArgs = {offering_id};

        Cursor cursor = db.query(TABLE_OFFERINGS, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;
    }


    public List<Offerings> getofferingdat(String offering_type) {
        List<Offerings> offeringslist = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        String selection = OFFERING_TYPE + " = ?";
        String[] selectionArgs = {offering_type};
        Cursor cursor = db.rawQuery("select " + OFFERING_ID + ", " + OFFERING_TYPE + ", " + OFFERING_NAME + ", " + PRICE + ", " + MIN_DOWN_PAYEMNT + ", " + DURATION + " from " + TABLE_OFFERINGS + " Where " + selection, selectionArgs);
        if (cursor.moveToFirst()) {
            do {
                String oid = cursor.getString(0);
                String otype = cursor.getString(1);
                String oname = cursor.getString(2);
                String oprice = cursor.getString(3);
                String odownpayment = cursor.getString(4);
                String oduration = cursor.getString(5);
                Offerings offerings = new Offerings(oid, otype, oname, oprice, odownpayment, oduration);
                offeringslist.add(offerings);


            } while (cursor.moveToNext());
        }

        return offeringslist;

    }

    public String getoffering(String offering_id) {
        String versionid = "1";
        SQLiteDatabase db = getReadableDatabase();
        String selection = OFFERING_ID + " = ?";
        String[] selectionArgs = {offering_id};
        Cursor cursor = db.rawQuery("select " + OFFERING_VERSION_ID + " from " + TABLE_OFFERINGS + " Where " + selection, selectionArgs);
        if (cursor.moveToFirst()) // Here we try to move to the first record
            versionid = String.valueOf(cursor.getString(0));
        cursor.close();
        db.close();
        return versionid;

    }


    //Transaction Table methods

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void addTransactions(Transaction transaction) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_ID_TRANS, transaction.getUser_Id());
        values.put(TRANS_TYPE, transaction.getTrans_type());
        values.put(PACK_IN_ID, transaction.getPack_in_id());
        values.put(PACK_OUT_ID, transaction.getPack_out_id());
        values.put(OFFERING_ID_TRANS, transaction.getOffering_id());
        values.put(ATTENDENT_ID, transaction.getAttendent_id());
        values.put(PAYMENT_AMOUNT, transaction.getPayment_amount());
        values.put(TRANSACTION_TYPE_ENUM, transaction.getTransaction_type_enum());


        db.insert(TABLE_TRANSACTION, null, values);
        db.close();

    }

    public void transactionJazakit(Transaction transaction) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(USER_ID_TRANS, transaction.getUser_Id());
        values.put(TRANS_TYPE, transaction.getTrans_type());
        //values.put(PACK_IN_ID,transaction.getPack_in_id());
        values.put(PACK_OUT_ID, transaction.getPack_out_id());
        values.put(PAYMENT_AMOUNT, transaction.getPayment_amount());
        values.put(OFFERING_ID_TRANS, transaction.getOffering_id());
        //values.put(ATTENDENT_ID,transaction.getAttendent_id());
        // values.put(TRANSACTION_TYPE_ENUM,transaction.getTransaction_type_enum());
        // values.put(CREATED_AT,getDateTime());
        //values.put(UPDATED_AT,getDateTime());


        // Inserting Row
        db.insert(TABLE_TRANSACTION, null, values);
        db.close();
    }

    public int countmaxtransid() {
        JSONObject publishList = new JSONObject();
        int x = 0;

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select " + " count(trans_id) " + " from " + TABLE_TRANSACTION, null);
        if (cursor.moveToFirst()) {

            rowExists = true;// Here we try to move to the first record

            if (cursor.getString(0) == null) {
                x = 0;
            } else {
                x = (Integer.parseInt(cursor.getString(0)));
            }

        }
        cursor.moveToNext();
        cursor.close();
        db.close();
        return x;
    }

    ///Publish log tables and methods

    public void addpublish(Publish publish) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(EVENT, publish.getEvent());
        values.put(DATA, publish.getData());
        values.put(DEVICE_ID, publish.getDevice_id());
        values.put(APP_VERSION, publish.getApp_version());
        values.put(String.valueOf(SUCCESSFUL), publish.getSuccessful());
        values.put(RESPONSE, publish.getResponse());
        // Inserting Row
        db.insert(TABLE_PUBLISH, null, values);
        db.close();
    }


    public int lastpub() {
        int publish_id = 1;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select max(pub_id) from " + TABLE_PUBLISH, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                rowExists = true;// Here we try to move to the first record

                if (cursor.getString(0) == null) {
                    publish_id = 1;

                } else {
                    publish_id = (Integer.parseInt(cursor.getString(0)) + 1);

                }
            } else {
                publish_id = 1;
            }

            return publish_id;


        } else {
            return publish_id;

        }


    }


    //offerings Item table queries


    public ArrayList<Integer> findInventoryId(String offering_id) {
        ArrayList arrayList = new ArrayList();
        String[] columns = {INVENTORY_ID};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = OFFERING_ITEM_OFF_ID + " = ?";
        String[] selectionArgs = {offering_id};

        Cursor cursor = db.query(TABLE_OFFERING_ITEMS, columns, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) {
            do { // Here we try to move to the first record
                int inventory_id = Integer.parseInt(cursor.getString(0));

                Offering_item offering_item = new Offering_item(inventory_id);
                arrayList.add(offering_item);
                // Only assign string value if we moved to first record
            } while (cursor.moveToNext());


        }
        db.close();
        return arrayList;
    }

    public int updatedeinventoryamount(Inventory inventory) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(INVENTORY_AMOUNT, inventory.getInventory_amount());

        return db.update(TABLE_INVENTORY, values, INVENTORY_ID + " = ? ", new String[]{String.valueOf(inventory.getInventory_id())});

    }


    public int findInventoryAmount(int inventoryid) {
        int amount = 0;
        String[] columns = {AMOUNT};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = INVENTORY_ID + " = ?";
        String[] selectionArgs = {String.valueOf(inventoryid)};

        Cursor cursor = db.query(TABLE_OFFERING_ITEMS, columns, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) { // Here we try to move to the first record
            amount = Integer.parseInt((cursor.getString(0)));
        }

        // Onlry assign string value if we moved to first record
        db.close();
        return amount;

    }

    //Publish Table

    public JSONObject getAllPublish() throws JSONException {
        JSONObject publishList = new JSONObject();
        String x = null;

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select " + EVENT + ", " + DATA + ", " + DEVICE_ID + ", " + APP_VERSION + " from " + TABLE_PUBLISH + " Where " + SUCCESSFUL + " = 0 ", null);
        if (cursor.moveToFirst()) {


          //  publishList.put("Pub_id", String.valueOf(cursor.getString(0)));
            publishList.put("event", cursor.getString(0));
            publishList.put("data", cursor.getString(1));
            publishList.put("device_id", cursor.getString(2));
            publishList.put("publish_at", System.currentTimeMillis() / 1000L);
            publishList.put("app_version", cursor.getString(3));
            // publishList.put("published_at","xyz");


        }
        cursor.close();
        return publishList;
    }

    public JSONObject getSpecificPublish(int pubids) throws JSONException {
        JSONObject publishList = new JSONObject();
        String x = null;

        SQLiteDatabase db = getReadableDatabase();
        String[] selectionArgs = {String.valueOf(pubids)};

        Cursor cursor = db.rawQuery("select " + PUB_ID + ", " + EVENT + ", " + DATA + ", " + DEVICE_ID + ", " + APP_VERSION + " from " + TABLE_PUBLISH + " Where " + PUB_ID + " = ? ", selectionArgs);
        if (cursor.moveToFirst()) {


            publishList.put("Pub_id", String.valueOf(cursor.getString(0)));
            publishList.put("event", cursor.getString(1));
            publishList.put("data", cursor.getString(2));
            publishList.put("device_id", cursor.getString(3));
            publishList.put("app_version", cursor.getString(4));
            // publishList.put("published_at","xyz");


        }
        cursor.close();
        return publishList;
    }


    public int updatedesuccusful(Publish publish) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SUCCESSFUL, publish.getSuccessful());

        return db.update(TABLE_PUBLISH, values, PUB_ID + " = ? ", new String[]{String.valueOf(publish.getPub_id())});

    }


    public int countPub() {
        JSONObject publishList = new JSONObject();
        int x = 0;

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select " + " count(pub_id) " + " from " + TABLE_PUBLISH + " Where " + SUCCESSFUL + " = 0 ", null);
        if (cursor.moveToFirst()) {

            rowExists = true;// Here we try to move to the first record

            if (cursor.getString(0) == null) {
                x = 0;
            } else {
                x = (Integer.parseInt(cursor.getString(0)));
            }

        }
        cursor.moveToNext();
        db.close();
        cursor.close();
        return x;
    }

    public boolean checkPublish() {
        String[] columns = {PUB_ID, SUCCESSFUL, DATA};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = SUCCESSFUL + " = 0";
        String[] selectionArgs = {PUB_ID};

        Cursor cursor = db.rawQuery("select " + PUB_ID + " from " + TABLE_PUBLISH + " Where " + SUCCESSFUL + " = 0 ", null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;

    }


    //Inverntory log table methods


    public void addInventorylog(Inventory_log inventory_log) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(ILOGS_INVENTORY_ID, inventory_log.getInventory_id());
        values.put(ILOG_TYPE, inventory_log.getLog_type());
        values.put(CLOUD_LOG_ID, inventory_log.getCloud_log_id());
        values.put(DELTA, inventory_log.getDelta());


        db.insert(TABLE_INVENTORY_LOG, null, values);
        db.close();

    }


    public int lastInventoryLogid() {
        int remittance_id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT min(start) FROM (SELECT l." + CLOUD_LOG_ID + "+ 1 AS START\n" + "FROM " + TABLE_INVENTORY_LOG + " AS l \n" + "  LEFT OUTER JOIN " + TABLE_INVENTORY_LOG + " AS r ON l." + CLOUD_LOG_ID + " + 1 = r." + CLOUD_LOG_ID + "\n" + "WHERE r." + CLOUD_LOG_ID + " IS null) a;", null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                rowExists = true;// Here we try to move to the first record

                if (cursor.getString(0) == null) {
                    remittance_id = 0;
                    db.close();
                } else {
                    remittance_id = (Integer.parseInt(cursor.getString(0)));
                    db.close();
                }
            } else {
                remittance_id = 0;
            }
            return remittance_id;

        } else {
            return remittance_id;

        }

    }


    public boolean checkInventory_id(String inventory_id) {

        String[] columns = {CLOUD_LOG_ID};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = CLOUD_LOG_ID + " = ?";
        String[] selectionArgs = {inventory_id};

        Cursor cursor = db.query(TABLE_INVENTORY_LOG, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;

    }


    public String findinventorydelta(String inventoryid) {
        String delta = "0";
        String[] columns = {DELTA};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = INVENTORY_ID + " = ?";
        String[] selectionArgs = {inventoryid};

        Cursor cursor = db.query(TABLE_INVENTORY, columns, selection, selectionArgs, null, null, null);
        if (cursor.moveToFirst()) { // Here we try to move to the first record
            delta = String.valueOf(cursor.getString(0));
            if (delta == null) {
                delta = "0";
            }
        }
        db.close();
        // Onlry assign string value if we moved to first record
        return delta;

    }


    public int countInventoryid() {
        JSONObject publishList = new JSONObject();
        int x = 0;

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select " + " count(inventory_id) " + " from " + TABLE_INVENTORY_LOG + " Where " + OFFERING_ID + " = ? ", null);
        if (cursor.moveToFirst()) {

            rowExists = true;// Here we try to move to the first record

            if (cursor.getString(0) == null) {
                x = 0;
            } else {
                x = (Integer.parseInt(cursor.getString(0)));
            }

        }
        cursor.moveToNext();
        cursor.close();
        db.close();
        return x;
    }

    public void addInventoryLogs(Inventory_log inventory_log) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(ILOGS_INVENTORY_ID, inventory_log.getInventory_id());
        values.put(ILOG_TYPE, inventory_log.getLog_type());
        values.put(ILOG_TRANS_ID, inventory_log.getTrans_id());
        values.put(DELTA, inventory_log.getDelta());


        db.insert(TABLE_INVENTORY_LOG, null, values);
        db.close();

    }


    //Remittance table methods

    public void addRemittance(Remittance remittance) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(REMITTANCE_ID, remittance.getRemiitance_id());
        values.put(REMITTANCE_TYPE, remittance.getRemittance_type());
        values.put(REMITTANCE_AMOUNT, remittance.getAmount());
        // values.put(REMITTANCE_DATE, remittance.getDate());
        values.put(REMITTANCE_SENT_TO, remittance.getSent_to());

        db.insert(TABLE_REMITTANCE, null, values);
        db.close();

    }

    public int lastRemittanceId() {
        int log_id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT min(start) FROM (SELECT l." + REMITTANCE_ID + " + 1 AS START \n " + "FROM " + TABLE_REMITTANCE + " AS l\n" + "  LEFT OUTER JOIN " + TABLE_REMITTANCE + " AS r ON l." + REMITTANCE_ID + "+ 1 = r." + REMITTANCE_ID + "\n" + "WHERE r." + REMITTANCE_ID + " IS null) a;", null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                rowExists = true;// Here we try to move to the first record

                if (cursor.getString(0) == null) {

                    log_id = 0;
                    db.close();
                } else {

                    log_id = (Integer.parseInt(cursor.getString(0)));
                    db.close();
                }
            } else {
                log_id = 0;
            }
            return log_id;

        } else {
            return log_id;

        }

    }

    public boolean checkRemittanceId(String remittance_id) {

        String[] columns = {REMITTANCE_ID};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = REMITTANCE_ID + " = ?";
        String[] selectionArgs = {remittance_id};

        Cursor cursor = db.query(TABLE_REMITTANCE, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;
    }


    //Methods for offering_items table

    public void addOffering_item(Offering_item offering_item) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(OFFERING_ITEM_OFF_ID, offering_item.getOffering_item_offering_id());
        values.put(INVENTORY_ID, offering_item.offering_item_Inventory_id);
        values.put(AMOUNT, offering_item.getOffering_item_amount());


        db.insert(TABLE_OFFERING_ITEMS, null, values);
        db.close();

    }


    public int maxofferingversionid() {
        int max_offering_version_id = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select max(offering_version_id) from " + TABLE_OFFERINGS, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                rowExists = true;// Here we try to move to the first record

                if (cursor.getString(0) == null) {
                    max_offering_version_id = 0;
                } else {
                    max_offering_version_id = (Integer.parseInt(cursor.getString(0)));
                    db.close();
                }
            } else {
                max_offering_version_id = 0;
            }
            return max_offering_version_id;

        } else {
            db.close();
            return max_offering_version_id;

        }

    }

    public void addAttendentSession(Attendent_Session attendent_session) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(ATT_ID, attendent_session.getAuto_att_id());
        values.put(TIME_IN, attendent_session.getTime_in());
        values.put(TIME_OUT, attendent_session.getTime_out());

        db.insert(TABLE_ATTENDANT_SESSION, null, values);
        db.close();

    }

    //Inventory table methods

    public int sumOfQuantity(int inve_id) {
        int rsult = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String[] selectionArgs = {String.valueOf(inve_id)};
        Cursor cursor = db.rawQuery("select " + " sum(delta) " + " from " + TABLE_INVENTORY_LOG + " Where " + INVENTORY_INVENTORY_ID + " = ?", selectionArgs);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                rowExists = true;// Here we try to move to the first record
                rsult = cursor.getInt(0);
                db.close();
            } else {
                rsult = 0;
            }
            cursor.close();
            db.close();
            return rsult;

        } else {
            cursor.close();
            db.close();
            return rsult;

        }
    }


    public int updatequantity(Inventory inventory) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(INVENTORY_AMOUNT, inventory.getInventory_amount());


        return db.update(TABLE_INVENTORY, values, INVENTORY_INVENTORY_ID + " = ? ", new String[]{String.valueOf(inventory.getInventory_id())});


    }


    public void addInventory(Inventory inventory) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(INVENTORY_INVENTORY_ID, inventory.getInventory_id());
        values.put(INVENTORY_NAME, inventory.getInventory_name());
        values.put(INVENTORY_AMOUNT, inventory.getInventory_amount());


        db.insert(TABLE_INVENTORY, null, values);
        db.close();

    }

    public boolean checkInventoryInventoryid(int inventory_id) {

        String[] columns = {INVENTORY_ID};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = INVENTORY_ID + " = ?";
        String[] selectionArgs = {String.valueOf(inventory_id)};

        Cursor cursor = db.query(TABLE_INVENTORY, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;

    }

    public boolean checkPackidInPacks(String packs_id) {

        String[] columns = {PACK_ID};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = PACK_ID + " = ?";
        String[] selectionArgs = {(packs_id)};

        Cursor cursor = db.query(TABLE_PACKS, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;

    }

    public void addPackStatus(PackStatus packStatus) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(PACK_ID, packStatus.getPacks_id());
        values.put(PACK_STATE, packStatus.getPack_state());
        values.put(LAST_USER_ID, packStatus.getLast_user_id());
        values.put(LASTTRANSACTION_DATE, packStatus.getLast_transaction_date());

        db.insert(TABLE_PACKS, null, values);
        db.close();

    }

    public int updatePackStatus(PackStatus packStatus) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(PACK_STATE, packStatus.getPack_state());
        values.put(LAST_USER_ID, packStatus.getLast_user_id());
        values.put(LASTTRANSACTION_DATE, packStatus.getLast_transaction_date());

        return db.update(TABLE_PACKS, values, PACK_ID + " = ? ", new String[]{String.valueOf(packStatus.getPacks_id())});

    }


    //HUB PROPERTIES TABLE


    public void addHubProperties(HubProperties hubProperties) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(HUB_ID_IMEI, hubProperties.getHub_imei());


        db.insert(TABLE_HUB_PROPERTIES, null, values);
        db.close();

    }

    public int packsInCount(String packstate) {
        int cusrsonnum = 0;
        String[] columns = {PACK_STATE};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = PACK_STATE + " = ?";
        String[] selectionArgs = {(packstate)};

        Cursor cursor = db.query(TABLE_PACKS, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            cusrsonnum = cursorCount;
            return cursorCount;
        }
        return cursorCount;

    }

    public int countUsers() {
        int rsult = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        // String[] selectionArgs = {String.valueOf(inve_id)};
        Cursor cursor = db.rawQuery("select " + " count(*)" + " from " + TABLE_USER, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return cursorCount;
        } else return rsult;
    }

    public int countPacks() {
        int rsult = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        // String[] selectionArgs = {String.valueOf(inve_id)};
        Cursor cursor = db.rawQuery("select " + " count(pack_id)" + " from " + TABLE_PACKS, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return cursorCount;
        } else return rsult;
    }

    public int countPublishes() {
        int rsult = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        // String[] selectionArgs = {String.valueOf(inve_id)};
        Cursor cursor = db.rawQuery("select " + " count(pub_id)" + " from " + TABLE_PUBLISH, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return cursorCount;
        } else return rsult;
    }

    public int countPublishesLogs() {
        int rsult = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        // String[] selectionArgs = {String.valueOf(inve_id)};
        Cursor cursor = db.rawQuery("select " + " count(publish_backlog_id)" + " from " + TABLE_PUBLISH_BACKLOG, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return cursorCount;
        } else return rsult;
    }


    public int packsOutCount(String packstate) {
        int cusrsonnum = 0;
        String[] columns = {PACK_STATE};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = PACK_STATE + " = ?";
        String[] selectionArgs = {(packstate)};

        Cursor cursor = db.query(TABLE_PACKS, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            cusrsonnum = cursorCount;
            return cursorCount;
        }
        return cursorCount;

    }


    public String lastRemittanceAtaHub() {
        String lastrem = "0";

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select " + REMITTANCE_AMOUNT + " from " + TABLE_REMITTANCE + " Where " + REMITTANCE_ID + "=" + " ( select max(" + REMITTANCE_ID + " ) " + " from " + TABLE_REMITTANCE + ")", null);
        if (cursor.moveToFirst()) {

            rowExists = true;// Here we try to move to the first record

            if (cursor.getString(0) == null) {
                lastrem = "0";
            } else {
                lastrem = (cursor.getString(0));
            }

        }
        cursor.moveToNext();
        cursor.close();
        db.close();
        return lastrem;
    }


    public String lastRemittanceDate() {
        String lastrem = "0";

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select " + REMITTANCE_DATE + " from " + TABLE_REMITTANCE + " Where " + REMITTANCE_ID + "=" + " ( select max(" + REMITTANCE_ID + " ) " + " from " + TABLE_REMITTANCE + ")", null);
        if (cursor.moveToFirst()) {

            rowExists = true;// Here we try to move to the first record

            if (cursor.getString(0) == null) {
                lastrem = "0";
            } else {
                lastrem = (cursor.getString(0));
            }

        }
        cursor.moveToNext();
        cursor.close();
        db.close();
        return lastrem;
    }


    public String lastCashAtHub(String deviceid) {
        String lastrem = "0";

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select " + TOTAL_CASH_IN + " from " + TABLE_HUB_PROPERTIES + " Where " + HUB_ID_IMEI + " = ? ", new String[]{String.valueOf(deviceid)});
        if (cursor.moveToFirst()) {

            rowExists = true;// Here we try to move to the first record

            if (cursor.getString(0) == null) {
                lastrem = "0";
            } else {
                lastrem = (cursor.getString(0));
            }

        }
        cursor.moveToNext();
        cursor.close();
        db.close();
        return lastrem;
    }


    public String totalCashIn(String deviceid) {
        String lastrem = "0";

        SQLiteDatabase db = getReadableDatabase();


        Cursor cursor = db.rawQuery("select " + TOTAL_CASH_IN + " from " + TABLE_HUB_PROPERTIES + " Where " + HUB_ID_IMEI + " = ? ", new String[]{String.valueOf(deviceid)});
        if (cursor.moveToFirst()) {

            rowExists = true;// Here we try to move to the first record

            if (cursor.getString(0) == null) {
                lastrem = "0";
            } else {
                lastrem = (cursor.getString(0));
            }

        }
        cursor.moveToNext();
        cursor.close();
        db.close();
        return lastrem;
    }


    public int updateCashAtHub(HubProperties hubProperties) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TOTAL_CASH_IN, hubProperties.getCash_at_hub());

        return db.update(TABLE_HUB_PROPERTIES, values, HUB_ID_IMEI + " = ? ", new String[]{String.valueOf(hubProperties.getHub_imei())});

    }

    public int updateHubPropertiesValue(HubProperties hubProperties) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CASH_AT_HUB, hubProperties.getCash_at_hub());
        values.put(LAST_REMIITANCE_AMOUNT, hubProperties.getLast_remittance_amount());
        values.put(LAST_REMIITANCE_DATE, hubProperties.getLastremittance_date());
        values.put(NUM_USERS, hubProperties.getNum_user());
        values.put(NUM_PACKS, hubProperties.getNum_packs());
        values.put(NUM_PUBLISHES, hubProperties.getNum_publishes());
        values.put(NUM_PUBLISH_LOG, hubProperties.getNum_publish_log());
        values.put(PACKS_OUT, hubProperties.getPacks_out());
        values.put(PACK_IN, hubProperties.getPacks_in());
        values.put(OFFERING_VERSION_ID, hubProperties.getOffering_version_id());
        values.put(MAX_REMITTANCE_ID, hubProperties.getMax_remittance_id());


        return db.update(TABLE_HUB_PROPERTIES, values, HUB_ID_IMEI + " = ? ", new String[]{String.valueOf(hubProperties.getHub_imei())});

    }


    //Publishlog method

    public String updatepublishLog() {
        String publish = "0";
        String[] columns = {PUB_ID, SUCCESSFUL, DATA};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = SUCCESSFUL + " = 0";
        String[] selectionArgs = {PUB_ID};

        Cursor cursor = db.rawQuery("select " + PUB_ID + " from " + TABLE_PUBLISH + " Where " + SUCCESSFUL + " = 0 ", null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            publish = cursor.getString(0);
            return publish;
        }
        return publish;

    }


    public void addpulishlog(PublishLog publishLog) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(PUBLISH_BACKLOG_ID, publishLog.getPublishbacklogid());


        db.insert(TABLE_PUBLISH_BACKLOG, null, values);
        db.close();

    }

    public boolean checkPublishLog(String pubid) {

        String[] columns = {PUBLISH_BACKLOG_ID};
        SQLiteDatabase db = this.getWritableDatabase();
        String selection = PUBLISH_BACKLOG_ID + " = ?";
        String[] selectionArgs = {(pubid)};

        Cursor cursor = db.query(TABLE_PUBLISH_BACKLOG, columns, selection, selectionArgs, null, null, null);
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;

    }

    public int deletepublishlog(String publish) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_PUBLISH_BACKLOG, PUBLISH_BACKLOG_ID + "= ?", new String[]{publish});
    }


    //Public Delete Queries

    public void deleteOfferings() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_OFFERINGS, null,null);
        db.close();

    }

    public void deleteInventory() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_OFFERING_ITEMS, null,null);
        db.close();

    }


    public String getImei() {
        String imei = "0";
        String[] columns = {HUB_ID_IMEI};
        SQLiteDatabase db = this.getWritableDatabase();
        //String selection = BALANCE + " IS NOT NULL OR " + BALANCE + " = 0 " + USER_ID + " = " + user_id;



        Cursor cursor = db.rawQuery("select " + HUB_ID_IMEI + " from " + TABLE_HUB_PROPERTIES , null,null);
        if (cursor.moveToFirst()) {
            imei = cursor.getString(0);

        }
        cursor.close();
        db.close();
        return  imei;
    }
}






























